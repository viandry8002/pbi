<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Global_Model extends CI_Model {

    // BIAYA INVESTASI

    // funtion subtotal pengadaaan lahan
    public function pengadaanLahan()
    {
        // sub total pengadaan lahan
        $pengadaanlahan = 'SELECT SUM(subtotal) AS stpl FROM investment_costs_detail WHERE category_id = 1';
        return $this->db->query($pengadaanlahan)->row()->stpl;
    }

    // function subtotal bangunan
    public function bangunan()
    {
        $bangunan = 'SELECT SUM(subtotal) AS stb FROM investment_costs_detail WHERE category_id = 2';
        return $this->db->query($bangunan)->row()->stb;
    }

    // function subtotal mesin dan peralatan
    public function mesinPeralatan()
    {
        $mesin = 'SELECT SUM(subtotal) AS stm FROM investment_costs_detail WHERE category_id = 3';
        return $this->db->query($mesin)->row()->stm;
    }

    // function subtotal fasilitas
    public function fasilitas()
    {
        $fasilitas = 'SELECT SUM(subtotal) AS stf FROM investment_costs_detail WHERE category_id = 4';
        return $this->db->query($fasilitas)->row()->stf;
    }

    // function subtotal kendaraan
    public function kendaraan()
    {
        $kendaraan = 'SELECT SUM(subtotal) AS stk FROM investment_costs_detail WHERE category_id = 5';
        return $this->db->query($kendaraan)->row()->stk;
    }

    // function subtotal prainvestasi
    public function prainvestasi()
    {
        $prainvest = 'SELECT SUM(subtotal) AS stp FROM investment_costs_detail WHERE category_id = 6';
        return $this->db->query($prainvest)->row()->stp;
    }

    // function total investasi
    public function totalInvestasi($stpengadaan_lahan, $stbangunan, $stmesin, $stfasilitas, $stkendaraan, $stprainvest)
    {
        $totalInvest = $stpengadaan_lahan + $stbangunan + $stmesin + $stfasilitas + $stkendaraan + $stprainvest;
        return $totalInvest;
    }

    // function modal kerja
    public function modalKerja()
    {
        return $this->db->get_where('investment_costs_detail', ['category_id' => 7])->row_array();
    }

    // function subtotal kotingensi
    public function kotingensi($totalinvestasi, $modalkerja)
    {
        $kotingensi = ($totalinvestasi + $modalkerja) * 0.1;
        return $kotingensi;
    }

    // function total biaya investasi
    public function totalBiayaInvest($totalinvestasi, $modalkerja, $kotingensi)
    {
        $totalbiayainvest = $totalinvestasi + $modalkerja + $kotingensi;
        return $totalbiayainvest;
    }

    // BIAYA PENYUSUTAN

    // function sub total biaya penyusutan nbth
    public function biayaPenyusutanNbth()
    {
        $nbth = 'SELECT SUM(nb_th_10) AS stnbth FROM reduction_costs';
        return $this->db->query($nbth)->row()->stnbth;
    }

    public function biayaPenyusutanPsth()
    {
        $psth = 'SELECT SUM(ps_th_10) AS stpsth FROM reduction_costs';
        return $this->db->query($psth)->row()->stpsth;
    }

    // BAIAYA LAIN LAIN

    // function subtotal pemeliharaan
    public function pemeliharaan()
    {
        $pemeliharaan = 'SELECT SUM(costs_year) AS stp FROM additional_costs WHERE category_id = 8';
        return $this->db->query($pemeliharaan)->row()->stp;
    }

    // function subtotal asuransi
    public function asuransi()
    {
        $asuransi = 'SELECT SUM(costs_year) AS sta FROM additional_costs WHERE category_id = 9';
        return $this->db->query($asuransi)->row()->sta;
    }

    // function pajak bumi dan bangunan 1,5%
    public function pajakBumi($biayapajakinvestasi)
    {
        $pajakbumi = $biayapajakinvestasi * 0.015;
        return $pajakbumi;
    }

    // function total biaya lain lain
    public function totalBiayaLain($stpemeliharaan, $stasuransi, $biayaadministrasi, $biayapajaktahunan, $pemasaran)
    {
        $total = $stpemeliharaan + $stasuransi + $biayaadministrasi + $biayapajaktahunan + $pemasaran;
        return $total;
    }

    // BIAYA BAHAN BAKU PEMBANTU UTILITAS

    // function total biaya bahan baku
    public function biayaBahanBaku()
    {
        $bahanbaku = 'SELECT SUM(costs_year) AS bb FROM material_costs';
        return $this->db->query($bahanbaku)->row()->bb;
    }

    // BIAYA TENAGA KERJA

    // function total jumlah tk tidak langsung
    public function jumlahTkTidakLangsung()
    {
        $jmlTktidaklangsung = 'SELECT SUM(total) AS jmltktl FROM employee_costs WHERE category_id = 12';
        return $this->db->query($jmlTktidaklangsung)->row()->jmltktl;
    }
    // funtion subtotal biaya kerja tidak langsung
    public function tkTidakLangsung()
    {
        $tktidaklangsung = 'SELECT SUM(costs_year) AS tktl FROM employee_costs WHERE category_id = 12';
        return $this->db->query($tktidaklangsung)->row()->tktl;
    }
    
    // function total jumlah tk langsung
    public function jumlahTkLangsung()
    {
        $jmlTkLangsung = 'SELECT SUM(total) AS jmltkl FROM employee_costs WHERE category_id = 13';
        return $this->db->query($jmlTkLangsung)->row()->jmltkl;
    }

    // funtion subtotal biaya kerja langsung
    public function tkLangsung()
    {
        $tkLangsung = 'SELECT SUM(costs_year) AS tkl FROM employee_costs WHERE category_id = 13';
        return $this->db->query($tkLangsung)->row()->tkl;
    }

    // function sub total biaya tenag kerja
    public function StTenagaKerja($sttktidaklangsung, $sttklangsung) {
        $total = $sttktidaklangsung + $sttklangsung;
        return $total;
    }

    // BIAYA PRODUKSI

    // sub total biaya tetap new
    public function totalBiayaTetap($tktidaklangsung, $pemeliharaan, $asuransi, $pajakbumi, $pemasaran, $administrasi)
    {
        $biayatetap = $tktidaklangsung + $pemeliharaan + $asuransi + $pajakbumi + $pemasaran + $administrasi;
        return $biayatetap;
    }

    // function sub total PBB
    public function subPBB()
    {
        $data['biayapajakinvestasi'] = $this->db->get_where('additional_costs', ['sub_category_id' => 2])->row_array(); // biaya investasi
        $total = $data['biayapajakinvestasi']['investment_costs'] * 0.015;
        return $total;
    }

    // function sub total biaya tetap
    public function biayaTetap1()
    {
        // tahun 1
        $biayatetap = 'SELECT SUM(year_1st) AS total FROM production_costs WHERE category_id = 14';
        return $this->db->query($biayatetap)->row()->total;
    }
    
    public function biayaTetap2()
    {
        // tahun 2
        $biayatetap = 'SELECT SUM(year_2nd) AS total FROM production_costs WHERE category_id = 14';
        return $this->db->query($biayatetap)->row()->total;
    }
    public function biayaTetap3()
    {
        // tahun 3
        $biayatetap = 'SELECT SUM(year_3rd) AS total FROM production_costs WHERE category_id = 14';
        return $this->db->query($biayatetap)->row()->total;
    }
    public function biayaTetap4()
    {
        // tahun 4
        $biayatetap = 'SELECT SUM(year_4rd) AS total FROM production_costs WHERE category_id = 14';
        return $this->db->query($biayatetap)->row()->total;
    }
    public function biayaTetap5()
    {
        // tahun 5
        $biayatetap = 'SELECT SUM(year_5rd) AS total FROM production_costs WHERE category_id = 14';
        return $this->db->query($biayatetap)->row()->total;
    }
    public function biayaTetap6()
    {
        // tahun 6
        $biayatetap = 'SELECT SUM(year_6rd) AS total FROM production_costs WHERE category_id = 14';
        return $this->db->query($biayatetap)->row()->total;
    }
    public function biayaTetap7()
    {
        // tahun 7
        $biayatetap = 'SELECT SUM(year_7rd) AS total FROM production_costs WHERE category_id = 14';
        return $this->db->query($biayatetap)->row()->total;
    }
    public function biayaTetap8()
    {
        // tahun 8
        $biayatetap = 'SELECT SUM(year_8rd) AS total FROM production_costs WHERE category_id = 14';
        return $this->db->query($biayatetap)->row()->total;
    }
    public function biayaTetap9()
    {
        // tahun 9
        $biayatetap = 'SELECT SUM(year_9rd) AS total FROM production_costs WHERE category_id = 14';
        return $this->db->query($biayatetap)->row()->total;
    }
    public function biayaTetap10()
    {
        // tahun 10
        $biayatetap = 'SELECT SUM(year_10rd) AS total FROM production_costs WHERE category_id = 14';
        return $this->db->query($biayatetap)->row()->total;
    }

    // sub total biaya tidak tetap tahun 1 NEW
    public function totalBiayaTidakTetap1($bakuinput, $tklangsung1)
    {
        $biayatidaktetap = $bakuinput + $tklangsung1;
        return $biayatidaktetap;
    }
    // sub total biaya tidak tetap tahun 2 NEW
    public function totalBiayaTidakTetap2($bakuinput, $tklangsung2)
    {
        $biayatidaktetap = $bakuinput + $tklangsung2;
        return $biayatidaktetap;
    }
    // sub total biaya tidak tetap tahun 3 NEW
    public function totalBiayaTidakTetap3($bakuinput, $tklangsung)
    {
        $biayatidaktetap = $bakuinput + $tklangsung;
        return $biayatidaktetap;
    }

    // function sub total biaya tidak tetap
    public function biayaTidakTetap1()
    {
        // tahun 1
        $biayatidaktetap = 'SELECT SUM(year_1st) AS total FROM production_costs WHERE category_id = 15';
        return $this->db->query($biayatidaktetap)->row()->total;
    }
    public function biayaTidakTetap2()
    {
        // tahun 2
        $biayatidaktetap = 'SELECT SUM(year_2nd) AS total FROM production_costs WHERE category_id = 15';
        return $this->db->query($biayatidaktetap)->row()->total;
    }
    public function biayaTidakTetap3()
    {
        // tahun 3
        $biayatidaktetap = 'SELECT SUM(year_3rd) AS total FROM production_costs WHERE category_id = 15';
        return $this->db->query($biayatidaktetap)->row()->total;
        
    }
    public function biayaTidakTetap4()
    {
        // tahun 4
        $biayatidaktetap = 'SELECT SUM(year_4rd) AS total FROM production_costs WHERE category_id = 15';
        return $this->db->query($biayatidaktetap)->row()->total;
    }
    public function biayaTidakTetap5()
    {
        // tahun 5
        $biayatidaktetap = 'SELECT SUM(year_5rd) AS total FROM production_costs WHERE category_id = 15';
        return $this->db->query($biayatidaktetap)->row()->total;
    }
    public function biayaTidakTetap6()
    {
        // tahun 6
        $biayatidaktetap = 'SELECT SUM(year_6rd) AS total FROM production_costs WHERE category_id = 15';
        return $this->db->query($biayatidaktetap)->row()->total;
    }
    public function biayaTidakTetap7()
    {
        // tahun 7
        $biayatidaktetap = 'SELECT SUM(year_7rd) AS total FROM production_costs WHERE category_id = 15';
        return $this->db->query($biayatidaktetap)->row()->total;
    }
    public function biayaTidakTetap8()
    {
        // tahun 8
        $biayatidaktetap = 'SELECT SUM(year_8rd) AS total FROM production_costs WHERE category_id = 15';
        return $this->db->query($biayatidaktetap)->row()->total;
    }
    public function biayaTidakTetap9()
    {
        // tahun 9
        $biayatidaktetap = 'SELECT SUM(year_9rd) AS total FROM production_costs WHERE category_id = 15';
        return $this->db->query($biayatidaktetap)->row()->total;
    }
    public function biayaTidakTetap10()
    {
        // tahun 10
        $biayatidaktetap = 'SELECT SUM(year_10rd) AS total FROM production_costs WHERE category_id = 15';
        return $this->db->query($biayatidaktetap)->row()->total;
    }


    // sub total biaya produksi NEW
    public function totalBP1($totalbiayatetap, $totalbiayatidaktetap1)
    {
        // tahun 1
        $total = $totalbiayatetap + $totalbiayatidaktetap1;
        return $total;
    }
    public function totalBP2($totalbiayatetap, $totalbiayatidaktetap2)
    {
        // tahun 2
        $total = $totalbiayatetap + $totalbiayatidaktetap2;
        return $total;
    }
    public function totalBP3($totalbiayatetap, $totalbiayatidaktetap3)
    {
        // tahun 3
        $total = $totalbiayatetap + $totalbiayatidaktetap3;
        return $total;
    }

    // function subtotal biaya produksi
    public function totalBiayaProduksi1($bt1, $btt1)
    {
        // tahun1
        $total = $bt1 + $btt1;
        return $total;
    }
    public function totalBiayaProduksi2($bt2, $btt2)
    {
        // tahun2
        $total = $bt2 + $btt2;
        return $total;
    }
    public function totalBiayaProduksi3($bt3, $btt3)
    {
        // tahun3
        $total = $bt3 + $btt3;
        return $total;
    }
    public function totalBiayaProduksi4($bt4, $btt4)
    {
        // tahun4
        $total = $bt4 + $btt4;
        return $total;
    }
    public function totalBiayaProduksi5($bt5, $btt5)
    {
        // tahun5
        $total = $bt5 + $btt5;
        return $total;
    }
    public function totalBiayaProduksi6($bt6, $btt6)
    {
        // tahun6
        $total = $bt6 + $btt6;
        return $total;
    }
    public function totalBiayaProduksi7($bt7, $btt7)
    {
        // tahun7
        $total = $bt7 + $btt7;
        return $total;
    }
    public function totalBiayaProduksi8($bt8, $btt8)
    {
        // tahun8
        $total = $bt8 + $btt8;
        return $total;
    }
    public function totalBiayaProduksi9($bt9, $btt9)
    {
        // tahun9
        $total = $bt9 + $btt9;
        return $total;
    }
    public function totalBiayaProduksi10($bt10, $btt10)
    {
        // tahun10
        $total = $bt10 + $btt10;
        return $total;
    }

    // PROFIT ANALISIS

    public function AnalisRugiLaba()
    {
         // get total nilai tahun 1 - 1-
        $data['nilai1'] = $this->Global_Model->nilai1();
        $data['nilai2'] = $this->Global_Model->nilai2();
        $data['nilai3'] = $this->Global_Model->nilai3();
        $data['nilai4'] = $this->Global_Model->nilai4();
        $data['nilai5'] = $this->Global_Model->nilai5();
        $data['nilai6'] = $this->Global_Model->nilai6();
        $data['nilai7'] = $this->Global_Model->nilai7();
        $data['nilai8'] = $this->Global_Model->nilai8();
        $data['nilai9'] = $this->Global_Model->nilai9();
        $data['nilai10'] = $this->Global_Model->nilai10();
        // SUB TOTAL TAB BIAYA PRODUKSI
        // BIAYA TETAP
        // sub total tk.tidak langsung
        $data['tktidaklangsung'] = $this->Global_Model->tkTidakLangsung();
        // sub total pemeliharaan
        $data['pemeliharaan'] = $this->Global_Model->pemeliharaan();
        // sub total asuransi
        $data['asuransi'] = $this->Global_Model->asuransi();
        // sub total pbb
        $data['pajakbumi'] = $this->Global_Model->subPBB();
        // pemasaran
        $data['pemasaran'] = $this->db->get_where('additional_costs', ['category_id' => 11])->row_array();
        // administrasi
        $data['administrasi'] = $this->db->get_where('additional_costs', ['category_id' => 10])->row_array();
        // BIAYA TIDAk TETAP
        // sub total bahan baku dan input
        $data['bakuinput'] = $this->Global_Model->biayaBahanBaku();
        // sub total tk langsung
        $data['tklangsung'] = $this->Global_Model->tkLangsung();
        // sub total tk langsung tahun 1
        $data['tklangsung1'] = $data['tklangsung'] * 0.7;
        // sub total tk langsung tahun 2
        $data['tklangsung2'] = $data['tklangsung'] * 0.8;

        // get sub total biaya tetap
        $data['totalbiayatetap'] = $this->Global_Model->totalBiayaTetap($data['tktidaklangsung'], $data['pemeliharaan'], $data['asuransi'], $data['pajakbumi'], $data['pemasaran']['costs_year'], $data['administrasi']['costs_year']);
        // sub total biaya tidak tetap tahun 1
        $data['totalbiayatidaktetap1'] = $this->Global_Model->totalBiayaTidakTetap1($data['bakuinput'], $data['tklangsung1']);
        // sub total biaya tidak tetap tahun 2
        $data['totalbiayatidaktetap2'] = $this->Global_Model->totalBiayaTidakTetap2($data['bakuinput'], $data['tklangsung2']);
        // sub total biaya tidak tetap tahun 3
        $data['totalbiayatidaktetap3'] = $this->Global_Model->totalBiayaTidakTetap3($data['bakuinput'], $data['tklangsung']);

        // total biaya produksi 1
        $data['totalBP1'] = $this->Global_Model->totalBP1($data['totalbiayatetap'], $data['totalbiayatidaktetap1']);
        // total biaya produksi 2
        $data['totalBP2'] = $this->Global_Model->totalBP2($data['totalbiayatetap'], $data['totalbiayatidaktetap2']);
        // total biaya produksi 3
        $data['totalBP3'] = $this->Global_Model->totalBP3($data['totalbiayatetap'], $data['totalbiayatidaktetap3']);

        
        // get total biaya tetap
        $data['bt1'] = $this->Global_Model->biayaTetap1();
        $data['bt2'] = $this->Global_Model->biayaTetap2();
        $data['bt3'] = $this->Global_Model->biayaTetap3();
        $data['bt4'] = $this->Global_Model->biayaTetap4();
        $data['bt5'] = $this->Global_Model->biayaTetap5();
        $data['bt6'] = $this->Global_Model->biayaTetap6();
        $data['bt7'] = $this->Global_Model->biayaTetap7();
        $data['bt8'] = $this->Global_Model->biayaTetap8();
        $data['bt9'] = $this->Global_Model->biayaTetap9();
        $data['bt10'] = $this->Global_Model->biayaTetap10();
        // get total biaya tidak tetap
        $data['btt1'] = $this->Global_Model->biayaTidakTetap1();
        $data['btt2'] = $this->Global_Model->biayaTidakTetap2();
        $data['btt3'] = $this->Global_Model->biayaTidakTetap3();
        $data['btt4'] = $this->Global_Model->biayaTidakTetap4();
        $data['btt5'] = $this->Global_Model->biayaTidakTetap5();
        $data['btt6'] = $this->Global_Model->biayaTidakTetap6();
        $data['btt7'] = $this->Global_Model->biayaTidakTetap7();
        $data['btt8'] = $this->Global_Model->biayaTidakTetap8();
        $data['btt9'] = $this->Global_Model->biayaTidakTetap9();
        $data['btt10'] = $this->Global_Model->biayaTidakTetap10();
        // total biaya prduksi tahun1 - 10
        $data['bp1'] = $this->Global_Model->biayaProduksi1($data['bt1'], $data['btt1']);
        $data['bp2'] = $this->Global_Model->biayaProduksi2($data['bt2'], $data['btt2']);
        $data['bp3'] = $this->Global_Model->biayaProduksi3($data['bt3'], $data['btt3']);
        $data['bp4'] = $this->Global_Model->biayaProduksi4($data['bt4'], $data['btt4']);
        $data['bp5'] = $this->Global_Model->biayaProduksi5($data['bt5'], $data['btt5']);
        $data['bp6'] = $this->Global_Model->biayaProduksi6($data['bt6'], $data['btt6']);
        $data['bp7'] = $this->Global_Model->biayaProduksi7($data['bt7'], $data['btt7']);
        $data['bp8'] = $this->Global_Model->biayaProduksi8($data['bt8'], $data['btt8']);
        $data['bp9'] = $this->Global_Model->biayaProduksi9($data['bt9'], $data['btt9']);
        $data['bp10'] = $this->Global_Model->biayaProduksi10($data['bt10'], $data['btt10']);
        // total laba operasi tahun1-10
        $data['laba1'] = $this->Global_Model->labaOperasi1($data['nilai1'], $data['bp1']);
        $data['laba2'] = $this->Global_Model->labaOperasi2($data['nilai2'], $data['bp2']);
        $data['laba3'] = $this->Global_Model->labaOperasi3($data['nilai3'], $data['bp3']);
        $data['laba4'] = $this->Global_Model->labaOperasi4($data['nilai4'], $data['bp4']);
        $data['laba5'] = $this->Global_Model->labaOperasi5($data['nilai5'], $data['bp5']);
        $data['laba6'] = $this->Global_Model->labaOperasi6($data['nilai6'], $data['bp6']);
        $data['laba7'] = $this->Global_Model->labaOperasi7($data['nilai7'], $data['bp7']);
        $data['laba8'] = $this->Global_Model->labaOperasi8($data['nilai8'], $data['bp8']);
        $data['laba9'] = $this->Global_Model->labaOperasi9($data['nilai9'], $data['bp9']);
        $data['laba10'] = $this->Global_Model->labaOperasi10($data['nilai10'], $data['bp10']);
        // total biaya penyusutan tahun 1-10
        $data['penyusutan1'] = $this->Global_Model->biayaPenyusutanPsth();
        // laba sebelum pajak tahun1-10
        $data['lsp1'] = $this->Global_Model->LabaSebelumPajak1($data['laba1'], $data['penyusutan1']);
        $data['lsp2'] = $this->Global_Model->LabaSebelumPajak2($data['laba2'], $data['penyusutan1']);
        $data['lsp3'] = $this->Global_Model->LabaSebelumPajak3($data['laba3'], $data['penyusutan1']);
        $data['lsp4'] = $this->Global_Model->LabaSebelumPajak4($data['laba4'], $data['penyusutan1']);
        $data['lsp5'] = $this->Global_Model->LabaSebelumPajak5($data['laba5'], $data['penyusutan1']);
        $data['lsp6'] = $this->Global_Model->LabaSebelumPajak6($data['laba6'], $data['penyusutan1']);
        $data['lsp7'] = $this->Global_Model->LabaSebelumPajak7($data['laba7'], $data['penyusutan1']);
        $data['lsp8'] = $this->Global_Model->LabaSebelumPajak8($data['laba8'], $data['penyusutan1']);
        $data['lsp9'] = $this->Global_Model->LabaSebelumPajak9($data['laba9'], $data['penyusutan1']);
        $data['lsp10'] = $this->Global_Model->LabaSebelumPajak10($data['laba10'], $data['penyusutan1']);
        // pajak
        $data['pajak1'] = $this->Global_Model->pajak1($data['lsp1']);
        $data['pajak2'] = $this->Global_Model->pajak2($data['lsp2']);
        $data['pajak3'] = $this->Global_Model->pajak3($data['lsp3']);
        $data['pajak4'] = $this->Global_Model->pajak4($data['lsp4']);
        $data['pajak5'] = $this->Global_Model->pajak5($data['lsp5']);
        $data['pajak6'] = $this->Global_Model->pajak6($data['lsp6']);
        $data['pajak7'] = $this->Global_Model->pajak7($data['lsp7']);
        $data['pajak8'] = $this->Global_Model->pajak8($data['lsp8']);
        $data['pajak9'] = $this->Global_Model->pajak9($data['lsp9']);
        $data['pajak10'] = $this->Global_Model->pajak10($data['lsp10']);
        // laba bersih
        $data['lb1'] = $this->Global_Model->labaBersih1($data['lsp1'], $data['pajak1']);
        $data['lb2'] = $this->Global_Model->labaBersih2($data['lsp2'], $data['pajak2']);
        $data['lb3'] = $this->Global_Model->labaBersih3($data['lsp3'], $data['pajak3']);
        $data['lb4'] = $this->Global_Model->labaBersih4($data['lsp4'], $data['pajak4']);
        $data['lb5'] = $this->Global_Model->labaBersih5($data['lsp5'], $data['pajak5']);
        $data['lb6'] = $this->Global_Model->labaBersih6($data['lsp6'], $data['pajak6']);
        $data['lb7'] = $this->Global_Model->labaBersih7($data['lsp7'], $data['pajak7']);
        $data['lb8'] = $this->Global_Model->labaBersih8($data['lsp8'], $data['pajak8']);
        $data['lb9'] = $this->Global_Model->labaBersih9($data['lsp9'], $data['pajak9']);
        $data['lb10'] = $this->Global_Model->labaBersih10($data['lsp10'], $data['pajak10']);
        
        return $data;
    }

    // function subtotan nilai tahun1 - tahun 10
    
    public function nilai1()
    {
        $nilai = $this->db->get_where('profit_analysis', ['category_id' => 16])->row_array();
        $acceptance = $this->db->get_where('configuration', ['id' => 1])->row_array();
        $data = $nilai['1st_year'] * $acceptance['acceptance_value'];
        return $data;
    }
    public function nilai2()
    {
        $nilai = $this->db->get_where('profit_analysis', ['category_id' => 16])->row_array();
        $acceptance = $this->db->get_where('configuration', ['id' => 1])->row_array();
        $data = $nilai['2nd_year'] * $acceptance['acceptance_value'];
        return $data;
    }
    public function nilai3()
    {
        $nilai = $this->db->get_where('profit_analysis', ['category_id' => 16])->row_array();
        $acceptance = $this->db->get_where('configuration', ['id' => 1])->row_array();
        $data = $nilai['3rd_year'] * $acceptance['acceptance_value'];
        return $data;
    }
    public function nilai4()
    {
        $nilai = $this->db->get_where('profit_analysis', ['category_id' => 16])->row_array();
        $acceptance = $this->db->get_where('configuration', ['id' => 1])->row_array();
        $data = $nilai['4rd_year'] * $acceptance['acceptance_value'];
        return $data;
    }
    public function nilai5()
    {
        $nilai = $this->db->get_where('profit_analysis', ['category_id' => 16])->row_array();
        $acceptance = $this->db->get_where('configuration', ['id' => 1])->row_array();
        $data = $nilai['5rd_year'] * $acceptance['acceptance_value'];
        return $data;
    }
    public function nilai6()
    {
        $nilai = $this->db->get_where('profit_analysis', ['category_id' => 16])->row_array();
        $acceptance = $this->db->get_where('configuration', ['id' => 1])->row_array();
        $data = $nilai['6rd_year'] * $acceptance['acceptance_value'];
        return $data;
    }
    public function nilai7()
    {
        $nilai = $this->db->get_where('profit_analysis', ['category_id' => 16])->row_array();
        $acceptance = $this->db->get_where('configuration', ['id' => 1])->row_array();
        $data = $nilai['7rd_year'] * $acceptance['acceptance_value'];
        return $data;
    }
    public function nilai8()
    {
        $nilai = $this->db->get_where('profit_analysis', ['category_id' => 16])->row_array();
        $acceptance = $this->db->get_where('configuration', ['id' => 1])->row_array();
        $data = $nilai['8rd_year'] * $acceptance['acceptance_value'];
        return $data;
    }
    public function nilai9()
    {
        $nilai = $this->db->get_where('profit_analysis', ['category_id' => 16])->row_array();
        $acceptance = $this->db->get_where('configuration', ['id' => 1])->row_array();
        $data = $nilai['9rd_year'] * $acceptance['acceptance_value'];
        return $data;
    }
    public function nilai10()
    {
        $nilai = $this->db->get_where('profit_analysis', ['category_id' => 16])->row_array();
        $acceptance = $this->db->get_where('configuration', ['id' => 1])->row_array();
        $data = $nilai['10rd_year'] * $acceptance['acceptance_value'];
        return $data;
    }

    // function subtotal biaya produksi tahun 1-10
    public function biayaProduksi1($bt1, $btt1)
    {
        $total = $bt1 + $btt1;
        return $total;
    }
    public function biayaProduksi2($bt2, $btt2)
    {
        $total = $bt2 + $btt2;
        return $total;
    }
    public function biayaProduksi3($bt3, $btt3)
    {
        $total = $bt3 + $btt3;
        return $total;
    }
    public function biayaProduksi4($bt4, $btt4)
    {
        $total = $bt4 + $btt4;
        return $total;
    }
    public function biayaProduksi5($bt5, $btt5)
    {
        $total = $bt5 + $btt5;
        return $total;
    }
    public function biayaProduksi6($bt6, $btt6)
    {
        $total = $bt6 + $btt6;
        return $total;
    }
    public function biayaProduksi7($bt7, $btt7)
    {
        $total = $bt7 + $btt7;
        return $total;
    }
    public function biayaProduksi8($bt8, $btt8)
    {
        $total = $bt8 + $btt8;
        return $total;
    }
    public function biayaProduksi9($bt9, $btt9)
    {
        $total = $bt9 + $btt9;
        return $total;
    }
    public function biayaProduksi10($bt10, $btt10)
    {
        $total = $bt10 + $btt10;
        return $total;
    }

    // total laba operasi tahun1 - 10
    public function labaOperasi1($nilai1, $bp1)
    {
        $total = $nilai1 - $bp1;
        return $total;
    }
    public function labaOperasi2($nilai2, $bp2)
    {
        $total = $nilai2 - $bp2;
        return $total;
    }
    public function labaOperasi3($nilai3, $bp3)
    {
        $total = $nilai3 - $bp3;
        return $total;
    }
    public function labaOperasi4($nilai4, $bp4)
    {
        $total = $nilai4 - $bp4;
        return $total;
    }
    public function labaOperasi5($nilai5, $bp5)
    {
        $total = $nilai5 - $bp5;
        return $total;
    }
    public function labaOperasi6($nilai6, $bp6)
    {
        $total = $nilai6 - $bp6;
        return $total;
    }
    public function labaOperasi7($nilai7, $bp7)
    {
        $total = $nilai7 - $bp7;
        return $total;
    }
    public function labaOperasi8($nilai8, $bp8)
    {
        $total = $nilai8 - $bp8;
        return $total;
    }
    public function labaOperasi9($nilai9, $bp9)
    {
        $total = $nilai9 - $bp9;
        return $total;
    }
    public function labaOperasi10($nilai10, $bp10)
    {
        $total = $nilai10 - $bp10;
        return $total;
    }

    // function laba sebelum pajak
    public function LabaSebelumPajak1($laba1, $penyusutan)
    {
        $total = $laba1 - $penyusutan;
        return $total;
    }
    public function LabaSebelumPajak2($laba2, $penyusutan)
    {
        $total = $laba2 - $penyusutan;
        return $total;
    }
    public function LabaSebelumPajak3($laba3, $penyusutan)
    {
        $total = $laba3 - $penyusutan;
        return $total;
    }
    public function LabaSebelumPajak4($laba4, $penyusutan)
    {
        $total = $laba4 - $penyusutan;
        return $total;
    }
    public function LabaSebelumPajak5($laba5, $penyusutan)
    {
        $total = $laba5 - $penyusutan;
        return $total;
    }
    public function LabaSebelumPajak6($laba6, $penyusutan)
    {
        $total = $laba6 - $penyusutan;
        return $total;
    }
    public function LabaSebelumPajak7($laba7, $penyusutan)
    {
        $total = $laba7 - $penyusutan;
        return $total;
    }
    public function LabaSebelumPajak8($laba8, $penyusutan)
    {
        $total = $laba8 - $penyusutan;
        return $total;
    }
    public function LabaSebelumPajak9($laba9, $penyusutan)
    {
        $total = $laba9 - $penyusutan;
        return $total;
    }
    public function LabaSebelumPajak10($laba10, $penyusutan)
    {
        $total = $laba10 - $penyusutan;
        return $total;
    }
    
    // function pajak
    public function pajak1($lsp1)
    {
        $total = $lsp1 * 0.3;
        return $total;
    }
    public function pajak2($lsp2)
    {
        $total = $lsp2 * 0.3;
        return $total;
    }
    public function pajak3($lsp3)
    {
        $total = $lsp3 * 0.3;
        return $total;
    }
    public function pajak4($lsp4)
    {
        $total = $lsp4 * 0.3;
        return $total;
    }
    public function pajak5($lsp5)
    {
        $total = $lsp5 * 0.3;
        return $total;
    }
    public function pajak6($lsp6)
    {
        $total = $lsp6 * 0.3;
        return $total;
    }
    public function pajak7($lsp7)
    {
        $total = $lsp7 * 0.3;
        return $total;
    }
    public function pajak8($lsp8)
    {
        $total = $lsp8 * 0.3;
        return $total;
    }
    public function pajak9($lsp9)
    {
        $total = $lsp9 * 0.3;
        return $total;
    }
    public function pajak10($lsp10)
    {
        $total = $lsp10 * 0.3;
        return $total;
    }

    // function laba bersih
    public function labaBersih1($lsp1, $pajak1)
    {
        $total = $lsp1 - $pajak1;
        return $total;
    }
    public function labaBersih2($lsp2, $pajak2)
    {
        $total = $lsp2 - $pajak2;
        return $total;
    }
    public function labaBersih3($lsp3, $pajak3)
    {
        $total = $lsp3 - $pajak3;
        return $total;
    }
    public function labaBersih4($lsp4, $pajak4)
    {
        $total = $lsp4 - $pajak4;
        return $total;
    }
    public function labaBersih5($lsp5, $pajak5)
    {
        $total = $lsp5 - $pajak5;
        return $total;
    }
    public function labaBersih6($lsp6, $pajak6)
    {
        $total = $lsp6 - $pajak6;
        return $total;
    }
    public function labaBersih7($lsp7, $pajak7)
    {
        $total = $lsp7 - $pajak7;
        return $total;
    }
    public function labaBersih8($lsp8, $pajak8)
    {
        $total = $lsp8 - $pajak8;
        return $total;
    }
    public function labaBersih9($lsp9, $pajak9)
    {
        $total = $lsp9 - $pajak9;
        return $total;
    }
    public function labaBersih10($lsp10, $pajak10)
    {
        $total = $lsp10 - $pajak10;
        return $total;
    }

    // ARUS KAS PENGELUARAN DAN PEMASUKAN

    // function total kas masuk tahun0-10
    public function totalKasMasuk0($mdsendiri, $mdpinjaman)
    {
        $total = $mdsendiri + $mdpinjaman;
        return $total;
    }
    public function totalKasMasuk1($lb1, $penyusutan)
    {
        $total = $lb1 + $penyusutan;
        return $total;
    }
    public function totalKasMasuk2($lb2, $penyusutan)
    {
        $total = $lb2 + $penyusutan;
        return $total;
    }
    public function totalKasMasuk3($lb3, $penyusutan)
    {
        $total = $lb3 + $penyusutan;
        return $total;
    }
    public function totalKasMasuk4($lb4, $penyusutan)
    {
        $total = $lb4 + $penyusutan;
        return $total;
    }
    public function totalKasMasuk5($lb5, $penyusutan, $nilaisisa)
    {
        $total = $lb5 + $penyusutan + $nilaisisa;
        return $total;
    }
    public function totalKasMasuk6($lb6, $penyusutan)
    {
        $total = $lb6 + $penyusutan;
        return $total;
    }
    public function totalKasMasuk7($lb7, $penyusutan)
    {
        $total = $lb7 + $penyusutan;
        return $total;
    }
    public function totalKasMasuk8($lb8, $penyusutan)
    {
        $total = $lb8 + $penyusutan;
        return $total;
    }
    public function totalKasMasuk9($lb9, $penyusutan)
    {
        $total = $lb9 + $penyusutan;
        return $total;
    }
    public function totalKasMasuk10($lb10, $penyusutan, $nilaisisa10)
    {
        $total = $lb10 + $penyusutan + $nilaisisa10;
        return $total;
    }

    // function k pinjaman tahun1-10
    public function kpinjaman1()
    {
        return $this->db->get_where('loan_costs', ['year_of_loan' => 1])->row_array();
    }
    public function kpinjaman2()
    {
        return $this->db->get_where('loan_costs', ['year_of_loan' => 2])->row_array();
    }
    public function kpinjaman3()
    {
        return $this->db->get_where('loan_costs', ['year_of_loan' => 3])->row_array();
    }
    public function kpinjaman4()
    {
        return $this->db->get_where('loan_costs', ['year_of_loan' => 4])->row_array();
    }
    public function kpinjaman5()
    {
        return $this->db->get_where('loan_costs', ['year_of_loan' => 5])->row_array();
    }
    public function kpinjaman6()
    {
        return $this->db->get_where('loan_costs', ['year_of_loan' => 6])->row_array();
    }
    public function kpinjaman7()
    {
        return $this->db->get_where('loan_costs', ['year_of_loan' => 7])->row_array();
    }
    public function kpinjaman8()
    {
        return $this->db->get_where('loan_costs', ['year_of_loan' => 8])->row_array();
    }
    public function kpinjaman9()
    {
        return $this->db->get_where('loan_costs', ['year_of_loan' => 9])->row_array();
    }
    public function kpinjaman10()
    {
        return $this->db->get_where('loan_costs', ['year_of_loan' => 10])->row_array();
    }

    // function total kas keluar tahun1-10
    public function totalKasKeluar1($bp1, $kp1)
    {
        $total = $bp1 + $kp1;
        return $total;
    }
    public function totalKasKeluar2($bp2, $kp2)
    {
        $total = $bp2 + $kp2;
        return $total;
    }
    public function totalKasKeluar3($bp3, $kp3)
    {
        $total = $bp3 + $kp3;
        return $total;
    }
    public function totalKasKeluar4($bp4, $kp4)
    {
        $total = $bp4 + $kp4;
        return $total;
    }
    public function totalKasKeluar5($bp5, $kp5)
    {
        $total = $bp5 + $kp5;
        return $total;
    }
    public function totalKasKeluar6($bp6, $kp6)
    {
        $total = $bp6 + $kp6;
        return $total;
    }
    public function totalKasKeluar7($bp7, $kp7)
    {
        $total = $bp7 + $kp7;
        return $total;
    }
    public function totalKasKeluar8($bp8, $kp8)
    {
        $total = $bp8 + $kp8;
        return $total;
    }
    public function totalKasKeluar9($bp9, $kp9)
    {
        $total = $bp9 + $kp9;
        return $total;
    }
    public function totalKasKeluar10($bp10, $kp10)
    {
        $total = $bp10 + $kp10;
        return $total;
    }

    // function saldo kas
    public function saldoKas1($tkm1, $tkk1)
    {
        $total = $tkm1 - $tkk1;
        return $total;
    }
    public function saldoKas2($tkm2, $tkk2)
    {
        $total = $tkm2 - $tkk2;
        return $total;
    }
    public function saldoKas3($tkm3, $tkk3)
    {
        $total = $tkm3 - $tkk3;
        return $total;
    }
    public function saldoKas4($tkm4, $tkk4)
    {
        $total = $tkm4 - $tkk4;
        return $total;
    }
    public function saldoKas5($tkm5, $tkk5)
    {
        $total = $tkm5 - $tkk5;
        return $total;
    }
    public function saldoKas6($tkm6, $tkk6)
    {
        $total = $tkm6 - $tkk6;
        return $total;
    }
    public function saldoKas7($tkm7, $tkk7)
    {
        $total = $tkm7 - $tkk7;
        return $total;
    }
    public function saldoKas8($tkm8, $tkk8)
    {
        $total = $tkm8 - $tkk8;
        return $total;
    }
    public function saldoKas9($tkm9, $tkk9)
    {
        $total = $tkm9 - $tkk9;
        return $total;
    }
    public function saldoKas10($tkm10, $tkk10)
    {
        $total = $tkm10 - $tkk10;
        return $total;
    }

    // function saldo kas komulatif tahun2-10
    public function saldoKomulatif2($sk1, $sk2)
    {
        $total = $sk1 + $sk2;
        return $total;
    }
    public function saldoKomulatif3($skk2, $sk3)
    {
        $total = $skk2 + $sk3;
        return $total;
    }
    public function saldoKomulatif4($skk3, $sk4)
    {
        $total = $skk3 + $sk4;
        return $total;
    }
    public function saldoKomulatif5($skk4, $sk5)
    {
        $total = $skk4 + $sk5;
        return $total;
    }
    public function saldoKomulatif6($skk5, $sk6)
    {
        $total = $skk5 + $sk6;
        return $total;
    }
    public function saldoKomulatif7($skk6, $sk3)
    {
        $total = $skk6 + $sk3;
        return $total;
    }
    public function saldoKomulatif8($skk7, $sk8)
    {
        $total = $skk7 + $sk8;
        return $total;
    }
    public function saldoKomulatif9($skk8, $sk9)
    {
        $total = $skk8 + $sk9;
        return $total;
    }
    public function saldoKomulatif10($skk9, $sk10)
    {
        $total = $skk9 + $sk10;
        return $total;
    }

    // ANALISIS FINANSIAL
    public function analisisFinansial()
    {
        $data['configuration'] = $this->db->get_where('configuration', ['id' => 1])->row_array();
        $data['analisis'] = $this->Global_Model->AnalisRugiLaba();
        // nilai tahun1 - 10
        $data['analisis']['nilai1'];
        $data['analisis']['nilai2'];
        $data['analisis']['nilai3'];
        $data['analisis']['nilai4'];
        $data['analisis']['nilai2'];
        $data['analisis']['nilai5'];
        $data['analisis']['nilai6'];
        $data['analisis']['nilai7'];
        $data['analisis']['nilai8'];
        $data['analisis']['nilai9'];
        $data['analisis']['nilai10'];
        // biaya tetap
        $data['analisis']['totalbiayatetap'];
        $data['analisis']['bt1'];
        $data['analisis']['bt2'];
        $data['analisis']['bt3'];
        $data['analisis']['bt4'];
        $data['analisis']['bt2'];
        $data['analisis']['bt5'];
        $data['analisis']['bt6'];
        $data['analisis']['bt7'];
        $data['analisis']['bt8'];
        $data['analisis']['bt9'];
        $data['analisis']['bt10'];
        // biaya tidak tetap
        $data['analisis']['totalbiayatidaktetap1'];
        $data['analisis']['totalbiayatidaktetap2'];
        $data['analisis']['totalbiayatidaktetap3'];
        $data['analisis']['btt1'];
        $data['analisis']['btt2'];
        $data['analisis']['btt3'];
        $data['analisis']['btt4'];
        $data['analisis']['btt2'];
        $data['analisis']['btt5'];
        $data['analisis']['btt6'];
        $data['analisis']['btt7'];
        $data['analisis']['btt8'];
        $data['analisis']['btt9'];
        $data['analisis']['btt10'];
        // total biaya produksi
        $data['analisis']['totalBP1'];
        $data['analisis']['totalBP2'];
        $data['analisis']['totalBP3'];
        $data['analisis']['bp1'];
        $data['analisis']['bp2'];
        $data['analisis']['bp3'];
        $data['analisis']['bp4'];
        $data['analisis']['bp2'];
        $data['analisis']['bp5'];
        $data['analisis']['bp6'];
        $data['analisis']['bp7'];
        $data['analisis']['bp8'];
        $data['analisis']['bp9'];
        $data['analisis']['bp10'];
        // laba operasi new
        $data['lo1'] = $data['analisis']['nilai1'] - $data['analisis']['totalBP1'];
        $data['lo2'] = $data['analisis']['nilai2'] - $data['analisis']['totalBP2'];
        $data['lo3'] = $data['analisis']['nilai3'] - $data['analisis']['totalBP3'];
        $data['analisis']['laba1'];
        $data['analisis']['laba2'];
        $data['analisis']['laba3'];
        $data['analisis']['laba4'];
        $data['analisis']['laba2'];
        $data['analisis']['laba5'];
        $data['analisis']['laba6'];
        $data['analisis']['laba7'];
        $data['analisis']['laba8'];
        $data['analisis']['laba9'];
        $data['analisis']['laba10'];
        // penyusutan
        $data['analisis']['penyusutan1'];
        // laba sebelum pajak
        $data['lsp1'] = $data['lo1'] - $data['analisis']['penyusutan1'];
        $data['lsp2'] = $data['lo2'] - $data['analisis']['penyusutan1'];
        $data['lsp3'] = $data['lo3'] - $data['analisis']['penyusutan1'];
        $data['analisis']['lsp1'];
        $data['analisis']['lsp2'];
        $data['analisis']['lsp3'];
        $data['analisis']['lsp4'];
        $data['analisis']['lsp2'];
        $data['analisis']['lsp5'];
        $data['analisis']['lsp6'];
        $data['analisis']['lsp7'];
        $data['analisis']['lsp8'];
        $data['analisis']['lsp9'];
        $data['analisis']['lsp10'];
        // pajak
        $data['pajak1'] = $data['lsp1'] * $data['configuration']['percent_pajak'] / 100;
        $data['pajak2'] = $data['lsp2'] * $data['configuration']['percent_pajak'] / 100;
        $data['pajak3'] = $data['lsp3'] * $data['configuration']['percent_pajak'] / 100;
        
        $data['analisis']['pajak1'];
        $data['analisis']['pajak2'];
        $data['analisis']['pajak3'];
        $data['analisis']['pajak4'];
        $data['analisis']['pajak2'];
        $data['analisis']['pajak5'];
        $data['analisis']['pajak6'];
        $data['analisis']['pajak7'];
        $data['analisis']['pajak8'];
        $data['analisis']['pajak9'];
        $data['analisis']['pajak10'];
        // laba bersih
        $data['lb1'] = $data['lsp1'] - $data['pajak1'];
        $data['lb2'] = $data['lsp2'] - $data['pajak2'];
        $data['lb3'] = $data['lsp3'] - $data['pajak3'];
        
        $data['analisis']['lb1'];
        $data['analisis']['lb2'];
        $data['analisis']['lb3'];
        $data['analisis']['lb4'];
        $data['analisis']['lb2'];
        $data['analisis']['lb5'];
        $data['analisis']['lb6'];
        $data['analisis']['lb7'];
        $data['analisis']['lb8'];
        $data['analisis']['lb9'];
        $data['analisis']['lb10'];

        // value biaya investasi
        // sub total pengadaan lahan
        $data['stpengadaan_lahan'] = $this->Global_Model->pengadaanLahan();
        // sub total bangunan
        $data['stbangunan'] = $this->Global_Model->bangunan();
        // sub total mesin dan peralatan
        $data['stmesin'] = $this->Global_Model->mesinPeralatan();
        // sub total fasilitsa
        $data['stfasilitas'] = $this->Global_Model->fasilitas();
        // sub total kendaraan
        $data['stkendaraan'] = $this->Global_Model->kendaraan();
        // sub total pra investasi
        $data['stprainvest'] = $this->Global_Model->prainvestasi();
        // total investasi
        $data['totalinvestasi'] = $this->Global_Model->totalInvestasi($data['stpengadaan_lahan'], $data['stbangunan'], $data['stmesin'], $data['stfasilitas'], $data['stkendaraan'], $data['stprainvest']);
        // get total modal kerja
        $data['modalkerja'] = $this->Global_Model->modalKerja();
        // total kotingensi
        $data['kotingensi'] = $this->Global_Model->kotingensi($data['totalinvestasi'], $data['modalkerja']['subtotal']);
        // total biaya investasi
        $data['totalbaiayainvest'] = $this->Global_Model->totalBiayaInvest($data['totalinvestasi'], $data['modalkerja']['subtotal'], $data['kotingensi']);
        
        $data['mdsendiri'] = $this->db->get_where('cash_expenses_income', ['id' => 2])->row_array();
		// total kas masuk
		$mdpinjaman = $this->db->get_where('cash_expenses_income', ['sub_category_id' => 52])->row_array();
		$data['tkm0'] = $this->Global_Model->totalKasMasuk0($data['mdsendiri']['zero_year'], $mdpinjaman['zero_year']);

        return $data;
    }
}