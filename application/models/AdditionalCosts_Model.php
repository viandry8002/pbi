<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdditionalCosts_Model extends CI_Model {
    // method getAll
    public function getAll()
    {
        $query = "SELECT `ac`.*, `c`.`title` AS `title_c`, `cc`.`title` AS `title_cc`
                FROM `additional_costs` AS `ac` 
                JOIN `category` AS `c` ON `ac`.`category_id` = `c`.`id`
                JOIN `sub_category` AS `cc` ON `ac`.`sub_category_id` = `cc`.`id`
                ORDER BY `ac`.`id` DESC";

        return $this->db->query($query)->result_array();
    }

    // method get byid
    public function getById($id)
    {
        return $this->db->get_where('additional_costs', ['id' => $id])->row_array();
    }

    // method save
    public function save($data)
    {
        return $this->db->insert('additional_costs', $data);
    }

    // method update
    public function update($data, $id)
    {
        return $this->db->update('additional_costs', $data, ['id' => $id]);
    }

    // method delete
    public function delete($id)
    {
        return $this->db->delete('additional_costs', ['id' => $id]);
    }

    // get data num rows
    public function TotalAdditionalCosts() {
        return $this->db->get('additional_costs')->num_rows();
    }
}