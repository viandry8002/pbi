<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MaterialCosts_Model extends CI_Model {

    // method getAll
    public function getAll()
    {
        return $this->db->get('material_costs')->result_array();
    }

    // method getByid
    public function getById($id)
    {
        return $this->db->get_where('material_costs', ['id' => $id])->row_array();   
    }

    // method save
    public function save($data)
    {   
        return $this->db->insert('material_costs', $data);
    }

    // method update
    public function update($data, $id)
    {
        return $this->db->update('material_costs', $data, ['id' => $id]);
    }

    // method delete
    public function delete($id)
    {
        return $this->db->delete('material_costs', ['id' => $id]);
    }

    // get data num rows
    public function TotalMaterialCosts() {
        return $this->db->get('material_costs')->num_rows();
    }
}