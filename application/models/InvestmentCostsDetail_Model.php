<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class InvestmentCostsDetail_Model extends CI_Model {
    // method get
    public function getAll()
    {
        $query = "SELECT `icd`.*, `c`.`title` AS `title_c`, `sc`.`title` AS `title_sc`, `u`.`title` AS `title_u`
                FROM `investment_costs_detail` AS `icd` 
                JOIN `category` AS `c` ON `icd`.`category_id` = `c`.`id`
                JOIN `sub_category` AS `sc` ON `icd`.`sub_category_id` = `sc`.`id`
                JOIN `unit` AS `u` ON `icd`.`unit_id` = `u`.`id`
                ORDER BY `icd`.`id` DESC
                ";
        return $this->db->query($query)->result_array();
    }

    // get SubCategory
    public function getSubCategory()
    {
        return $this->db->get('sub_category')->result_array();
    }
    // get unit
    public function getUnit()
    {
        return $this->db->get('unit')->result_array();
    }

    // method getById
    public function getById($id)
    {
        return $this->db->get_where('investment_costs_detail', ['id' => $id])->row_array();
    }

    // meethod save
    public function save($data)
    {
        return $this->db->insert('investment_costs_detail', $data);
    }

    // method update
    public function update($data, $id)
    {
        return $this->db->update('investment_costs_detail', $data, ['id' => $id]);
    }

    // method delete
    public function delete($id)
    {
        return $this->db->delete('investment_costs_detail', ['id' => $id]);
    }

    // get data num rows
    public function TotalInvestCosts() {
        return $this->db->get('investment_costs_detail')->num_rows();
    }
}