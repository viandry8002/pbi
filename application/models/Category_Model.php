<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_Model extends CI_Model {
    // method get
    public function getAll()
    {
        return $this->db->get('category')->result_array();
    }

    // method getById
    public function getById($id)
    {
        return $this->db->get_where('category', ['id' => $id])->row_array();
    }

    // meethod save
    public function save()
    {
        date_default_timezone_set("Asia/Jakarta");
        $post = $this->input->post();
        $this->type = $post['type'];
        $this->title = $post['title'];
        $this->created_by = $post['created_by'] = 'admin';
        $this->created_at = date("y-m-d h:i:sa");
        $this->updated_at = date("y-m-d h:i:sa");

        return $this->db->insert('category', $this);
    }

    // method update
    public function update()
    {
        date_default_timezone_set("Asia/Jakarta");
        $post = $this->input->post();
        $this->id = $post['id'];
        $this->type = $post['type'];
        $this->title = $post['title'];
        $this->created_by = $post['created_by'];
        $this->created_at = $post['created_at'];
        $this->updated_at = date("y-m-d h:i:sa");

        return $this->db->update('category', $this, ['id' => $post['id']]);
    }

    // method delete
    public function delete($id)
    {
        return $this->db->delete('category', ['id' => $id]);
    }
}