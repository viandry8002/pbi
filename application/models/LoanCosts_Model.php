<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoanCosts_Model extends CI_Model {

    // method getAll
    public function getAll()
    {
        return $this->db->get('loan_costs')->result_array();
    }

    // method getByid
    public function getById($id)
    {
        return $this->db->get_where('loan_costs', ['id' => $id])->row_array();   
    }

    // method save
    public function save($data)
    { 
        return $this->db->insert('loan_costs', $data);
    }

    // method update
    public function update($data, $id)
    {
        return $this->db->update('loan_costs', $data, ['id' => $id]);
    }

    // method delete
    public function delete($id)
    {
        return $this->db->delete('loan_costs', ['id' => $id]);
    }

    // get data num rows
    public function TotalLoanCosts() {
        return $this->db->get('loan_costs')->num_rows();
    }
}