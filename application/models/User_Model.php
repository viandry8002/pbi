<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_Model extends CI_Model {
    // get all
    public function getAll()
    {
        $query = "SELECT `u`.*, `ur`.`role` AS `title_ur`
                FROM `user` AS `u` JOIN `user_role` AS `ur`
                ON `u`.`role_id` = `ur`.`id`
                ORDER BY `u`.`id` DESC";
        return $this->db->query($query)->result_array();
    }

    // method delete
    public function delete($id)
    {
        return $this->db->delete('user', ['id' => $id]);
    }
}