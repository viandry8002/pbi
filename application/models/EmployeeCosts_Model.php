<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EmployeeCosts_Model extends CI_Model {
    // method get
    public function getAll()
    {
        $query = "SELECT `ec`.*, `c`.`title` AS `title_c`, `sc`.`title` AS `title_sc`
                FROM `employee_costs` AS `ec` 
                JOIN `category` AS `c` ON `ec`.`category_id` = `c`.`id`
                JOIN `sub_category` AS `sc` ON `ec`.`sub_category_id` = `sc`.`id`
                ORDER BY `ec`.`id` DESC";

        return $this->db->query($query)->result_array();
    }

    // method getById
    public function getById($id)
    {
        return $this->db->get_where('employee_costs', ['id' => $id])->row_array();
    }

    // meethod save
    public function save($data)
    {
        return $this->db->insert('employee_costs', $data);
    }

    // method update
    public function update($data, $id)
    {
        return $this->db->update('employee_costs', $data, ['id' => $id]);
    }

    // method delete
    public function delete($id)
    {
        return $this->db->delete('employee_costs', ['id' => $id]);
    }

    // get data num rows
    public function TotalEmployeeCosts() {
        return $this->db->get('employee_costs')->num_rows();
    }
}