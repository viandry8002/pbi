<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configuration_Model extends CI_Model {

    // method get by id
    public function getById($id)
    {
        return $this->db->get_where('configuration', ['id' => $id])->row_array();
    }

    // method update 
    public function update($data, $id)
    {
        return $this->db->update('configuration', $data, ['id' => $id]);
    }

}