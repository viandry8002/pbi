<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProductionCosts_Model extends CI_Model {
    // method get
    public function getAll()
    {
        $query = "SELECT `pc`.*, `c`.`title` AS `title_c`, `sc`.`title` AS `title_sc`
                FROM `production_costs` AS `pc` 
                JOIN `category` AS `c` ON `pc`.`category_id` = `c`.`id`
                JOIN `sub_category` AS `sc` ON `pc`.`sub_category_id` = `sc`.`id`
                ORDER BY `pc`.`id` DESC";

        return $this->db->query($query)->result_array();
    }

    // method getById
    public function getById($id)
    {
        return $this->db->get_where('production_costs', ['id' => $id])->row_array();
    }

    // meethod save
    public function save($data)
    {
        return $this->db->insert('production_costs', $data);
    }

    // method update
    public function update($data, $id)
    {
        return $this->db->update('production_costs', $data, ['id' => $id]);
    }

    // method delete
    public function delete($id)
    {
        return $this->db->delete('production_costs', ['id' => $id]);
    }

    // get data num rows
    public function TotalProductionCosts() {
        return $this->db->get('production_costs')->num_rows();
    }
}