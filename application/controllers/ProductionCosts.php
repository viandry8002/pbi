<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProductionCosts extends CI_Controller {

	// consturct
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        // load model
        $this->load->model('ProductionCosts_Model');
        $this->load->model('Global_Model');
	}
	
    // function index productioncosts
	public function index()
	{
        // BIAYA TETAP
        // sub total tk.tidak langsung
        $data['tktidaklangsung'] = $this->Global_Model->tkTidakLangsung();
        // sub total pemeliharaan
        $data['pemeliharaan'] = $this->Global_Model->pemeliharaan();
        // sub total asuransi
        $data['asuransi'] = $this->Global_Model->asuransi();
        // sub total pbb
        $data['pajakbumi'] = $this->Global_Model->subPBB();
        // pemasaran
        $data['pemasaran'] = $this->db->get_where('additional_costs', ['category_id' => 11])->row_array();
        // administrasi
        $data['administrasi'] = $this->db->get_where('additional_costs', ['category_id' => 10])->row_array();
        // BIAYA TIDAk TETAP
        // sub total bahan baku dan input
        $data['bakuinput'] = $this->Global_Model->biayaBahanBaku();
        $data['bakuinput1'] = $data['bakuinput'] * 0.7;
        $data['bakuinput2'] = $data['bakuinput'] * 0.8;
        // sub total tk langsung
        $data['tklangsung'] = $this->Global_Model->tkLangsung();
        // sub total tk langsung tahun 1
        $data['tklangsung1'] = $data['tklangsung'] * 0.7;
        // sub total tk langsung tahun 2
        $data['tklangsung2'] = $data['tklangsung'] * 0.8;

        // get sub total biaya tetap
        $data['totalbiayatetap'] = $this->Global_Model->totalBiayaTetap($data['tktidaklangsung'], $data['pemeliharaan'], $data['asuransi'], $data['pajakbumi'], $data['pemasaran']['costs_year'], $data['administrasi']['costs_year']);
        // sub total biaya tidak tetap tahun 1
        $data['totalbiayatidaktetap1'] = $this->Global_Model->totalBiayaTidakTetap1($data['bakuinput1'], $data['tklangsung1']);
        // sub total biaya tidak tetap tahun 2
        $data['totalbiayatidaktetap2'] = $this->Global_Model->totalBiayaTidakTetap2($data['bakuinput2'], $data['tklangsung2']);
        // sub total biaya tidak tetap tahun 3
        $data['totalbiayatidaktetap3'] = $this->Global_Model->totalBiayaTidakTetap3($data['bakuinput'], $data['tklangsung']);

        // total biaya produksi 1
        $data['totalBP1'] = $this->Global_Model->totalBP1($data['totalbiayatetap'], $data['totalbiayatidaktetap1']);
        // total biaya produksi 2
        $data['totalBP2'] = $this->Global_Model->totalBP2($data['totalbiayatetap'], $data['totalbiayatidaktetap2']);
        // total biaya produksi 3
        $data['totalBP3'] = $this->Global_Model->totalBP3($data['totalbiayatetap'], $data['totalbiayatidaktetap3']);

        // BIAYA TETAP
        // get sub total biaya tetap tahun 1
        $data['bt1'] = $this->Global_Model->biayaTetap1();
        // get sub total biaya tetap tahun 2
        $data['bt2'] = $this->Global_Model->biayaTetap2();
        // get sub total biaya tetap tahun 3
        $data['bt3'] = $this->Global_Model->biayaTetap3();
        // get sub total biaya tetap tahun 4
        $data['bt4'] = $this->Global_Model->biayaTetap4();
        // get sub total biaya tetap tahun 5
        $data['bt5'] = $this->Global_Model->biayaTetap5();
        // get sub total biaya tetap tahun 6
        $data['bt6'] = $this->Global_Model->biayaTetap6();
        // get sub total biaya tetap tahun 7
        $data['bt7'] = $this->Global_Model->biayaTetap7();
        // get sub total biaya tetap tahun 8
        $data['bt8'] = $this->Global_Model->biayaTetap8();
        // get sub total biaya tetap tahun 9
        $data['bt9'] = $this->Global_Model->biayaTetap9();
        // get sub total biaya tetap tahun 10
        $data['bt10'] = $this->Global_Model->biayaTetap10();
        // BIAYA TIDAK TETAP
        // get sub total biaya tetap tahun 1
        $data['btt1'] = $this->Global_Model->biayaTidakTetap1();
        // get sub total biaya tetap tahun 2
        $data['btt2'] = $this->Global_Model->biayaTidakTetap2();
        // get sub total biaya tetap tahun 3
        $data['btt3'] = $this->Global_Model->biayaTidakTetap3();
        // get sub total biaya tetap tahun 4
        $data['btt4'] = $this->Global_Model->biayaTidakTetap4();
        // get sub total biaya tetap tahun 5
        $data['btt5'] = $this->Global_Model->biayaTidakTetap5();
        // get sub total biaya tetap tahun 6
        $data['btt6'] = $this->Global_Model->biayaTidakTetap6();
        // get sub total biaya tetap tahun 7
        $data['btt7'] = $this->Global_Model->biayaTidakTetap7();
        // get sub total biaya tetap tahun 8
        $data['btt8'] = $this->Global_Model->biayaTidakTetap8();
        // get sub total biaya tetap tahun 9
        $data['btt9'] = $this->Global_Model->biayaTidakTetap9();
        // get sub total biaya tetap tahun 10
        $data['btt10'] = $this->Global_Model->biayaTidakTetap10();
        // total biaya produksi
        $data['tbp1'] = $this->Global_Model->totalBiayaProduksi1($data['bt1'], $data['btt1']);
        $data['tbp2'] = $this->Global_Model->totalBiayaProduksi2($data['bt2'], $data['btt2']);
        $data['tbp3'] = $this->Global_Model->totalBiayaProduksi3($data['bt3'], $data['btt3']);
        $data['tbp4'] = $this->Global_Model->totalBiayaProduksi4($data['bt4'], $data['btt4']);
        $data['tbp5'] = $this->Global_Model->totalBiayaProduksi5($data['bt5'], $data['btt5']);
        $data['tbp6'] = $this->Global_Model->totalBiayaProduksi6($data['bt6'], $data['btt6']);
        $data['tbp7'] = $this->Global_Model->totalBiayaProduksi7($data['bt7'], $data['btt7']);
        $data['tbp8'] = $this->Global_Model->totalBiayaProduksi8($data['bt8'], $data['btt8']);
        $data['tbp9'] = $this->Global_Model->totalBiayaProduksi9($data['bt9'], $data['btt9']);
        $data['tbp10'] = $this->Global_Model->totalBiayaProduksi10($data['bt10'], $data['btt10']);

		// set data
		$data['title'] = 'Biaya Produksi';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
		$data['productioncosts'] = $this->ProductionCosts_Model->getAll();
		$data['category'] = $this->db->get_where('category', ['type' => 'biaya_produksi'])->result_array();
		// load view
		$this->load->view('templates/overview_header', $data);
		$this->load->view('templates/overview_sidebar');
		$this->load->view('templates/overview_topbar', $data);
		$this->load->view('productioncosts/index', $data);
		$this->load->view('templates/overview_footer');
	}

    // function list sub category
    public function listSubCategory()
    {
        $id_category = $this->input->post('id_category');
        $subcategory = $this->SubCategory_Model->ViewByCategory($id_category);

        $lists = "<option value=''>Pilih Sub Kategori</option>";

        foreach($subcategory as $data) {
            $lists .= "<option value='".$data['id']."'>".$data['title']."</option>";
        }

        $callback = array('list_sub_category' => $lists);

        echo json_encode($callback);
    }

	// add productioncosts
    public function add()
    {
        // validation
        $this->form_validation->set_rules('category_id', 'Kategori', 'required', [
            'required' => 'Kategori harus di isi!'
        ]);
        $this->form_validation->set_rules('sub_category_id', 'Sub Kategori', 'required', [
            'required' => 'Sub kategori harus di isi!'
        ]);

        if ($this->form_validation->run() == false) {
            // load view
            $this->load->view('templates/overview_header');
            $this->load->view('templates/overview_sidebar');
            $this->load->view('templates/overview_topbar');
            $this->load->view('productioncosts/index');
            $this->load->view('templates/overview_footer');
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Gagal Menambahkan Data!</div>');
            redirect('ProductionCosts');
        } else {
            // var post
            $post = $this->input->post();
            // set time zone
            date_default_timezone_set("Asia/Jakarta");
            // data array
            $data = [
                'category_id' => $post['category_id'],
                'sub_category_id' => $post['sub_category_id'],
                'year_1st' => $post['year_1st'],
                'year_2nd' => $post['year_2nd'],
                'year_3rd' => $post['year_3rd'],
                'year_4rd' => $post['year_4rd'],
                'year_5rd' => $post['year_5rd'],
                'year_6rd' => $post['year_6rd'],
                'year_7rd' => $post['year_7rd'],
                'year_8rd' => $post['year_8rd'],
                'year_9rd' => $post['year_9rd'],
                'year_10rd' => $post['year_10rd'],
                'created_at' => date("y-m-d h:i:sa"),
                'updated_at' => date("y-m-d h:i:sa"),
            ];
            // call model
            $this->ProductionCosts_Model->save($data);
            // set session
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Berhasil Menambahkan Data!</div>');
            // redirect
            redirect('ProductionCosts');
        }
    }

    // function edit
    public function edit($id = null)
    {
        // set data
        $data['title'] = "Ubah Biaya Produksi";
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $productioncosts = $this->ProductionCosts_Model;
        $data['productioncosts'] = $productioncosts->getById($id);
        $data['category'] = $this->db->get_where('category', ['type' => 'biaya_produksi'])->result_array();
        
        // validation id
        if (!isset($id)) redirect('ProductionCosts');
        if (!$data['productioncosts']) show_404();

        // validation
        $this->form_validation->set_rules('category_id', 'Kategori', 'required', [
            'required' => 'Kategori harus di isi!'
        ]);
        $this->form_validation->set_rules('sub_category_id', 'Sub Kategori', 'required', [
            'required' => 'Sub kategori harus di isi!'
        ]);

        if ($this->form_validation->run() == false) {
            // load view
            $this->load->view('templates/overview_header', $data);
            $this->load->view('templates/overview_sidebar');
            $this->load->view('templates/overview_topbar');
            $this->load->view('productioncosts/edit_productioncosts', $data);
            $this->load->view('templates/overview_footer');
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Data Gagal Diubah!</div>');
        } else {
            // var post
            $post = $this->input->post();
            // set time zone
            date_default_timezone_set("Asia/Jakarta");
            // data array
            $data = [
                'id' => $post['id'],
                'category_id' => $post['category_id'],
                'sub_category_id' => $post['sub_category_id'],
                'year_1st' => $post['year_1st'],
                'year_2nd' => $post['year_2nd'],
                'year_3rd' => $post['year_3rd'],
                'year_4rd' => $post['year_4rd'],
                'year_5rd' => $post['year_5rd'],
                'year_6rd' => $post['year_6rd'],
                'year_7rd' => $post['year_7rd'],
                'year_8rd' => $post['year_8rd'],
                'year_9rd' => $post['year_9rd'],
                'year_10rd' => $post['year_10rd'],
                'created_at' => $post['created_at'],
                'updated_at' => date("y-m-d h:i:sa"),
            ];
            // var id
            $id = $post['id'];
            // call model
            $productioncosts->update($data, $id);
            // set session
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data Berhasil Diubah!</div>');
            // redirect
            redirect('ProductionCosts');
        }
    }
    
    // delete productioncosts
    public function delete($id = null)
    {
        if (!isset($id)) show_404();
        $productioncosts = $this->ProductionCosts_Model;
        if ($productioncosts->delete($id)) {
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data Berhasil Di Hapus!</div>');
            redirect('ProductionCosts');
        }
    }

    // export to excel
    public function excel()
    {
        $data['productioncosts'] = $this->ProductionCosts_Model->getAll();
        include APPPATH.'PHPExcel-1.8/Classes/PHPExcel.php';
        include APPPATH.'PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php';

        $object = new PHPExcel();
        $object->getProperties()->setCreator("Biaya Produksi");
        $object->getProperties()->setLastModifiedBy("Biaya Produksi");
        $object->getProperties()->setTitle("Biaya Detail Produksi");

        $object->setActiveSheetIndex(0);

        $object->getActiveSheet()->setCellValue('A1', 'NO');
        $object->getActiveSheet()->setCellValue('B1', 'Kategori');
        $object->getActiveSheet()->setCellValue('C1', 'Sub Kategori');
        $object->getActiveSheet()->setCellValue('D1', 'Tahun 1');
        $object->getActiveSheet()->setCellValue('E1', 'Tahun 2');
        $object->getActiveSheet()->setCellValue('F1', 'Tahun 3');
        $object->getActiveSheet()->setCellValue('G1', 'Tahun 4');
        $object->getActiveSheet()->setCellValue('H1', 'Tahun 5');
        $object->getActiveSheet()->setCellValue('I1', 'Tahun 6');
        $object->getActiveSheet()->setCellValue('J1', 'Tahun 7');
        $object->getActiveSheet()->setCellValue('K1', 'Tahun 8');
        $object->getActiveSheet()->setCellValue('L1', 'Tahun 9');
        $object->getActiveSheet()->setCellValue('M1', 'Tahun 10');

        $baris = 2;
        $no = 1;

        foreach ($data['productioncosts'] as $pc) {
            $object->getActiveSheet()->setCellValue('A'.$baris, $no++);
            $object->getActiveSheet()->setCellValue('B'.$baris, $pc['title_c']);
            $object->getActiveSheet()->setCellValue('C'.$baris, $pc['title_sc']);
            $object->getActiveSheet()->setCellValue('D'.$baris, $pc['year_1st']);
            $object->getActiveSheet()->setCellValue('E'.$baris, $pc['year_2nd']);
            $object->getActiveSheet()->setCellValue('F'.$baris, $pc['year_3rd']);
            $object->getActiveSheet()->setCellValue('G'.$baris, $pc['year_4rd']);
            $object->getActiveSheet()->setCellValue('H'.$baris, $pc['year_5rd']);
            $object->getActiveSheet()->setCellValue('I'.$baris, $pc['year_6rd']);
            $object->getActiveSheet()->setCellValue('J'.$baris, $pc['year_7rd']);
            $object->getActiveSheet()->setCellValue('K'.$baris, $pc['year_8rd']);
            $object->getActiveSheet()->setCellValue('L'.$baris, $pc['year_9rd']);
            $object->getActiveSheet()->setCellValue('M'.$baris, $pc['year_10rd']);

            $baris++;
        }

        $filename = "Data_Biaya_Produksi".'.xlsx';

        $object->getActiveSheet()->setTitle("Data Biaya Produksi");

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'. $filename .'"'); 
		header('Cache-Control: max-age=0');

        $writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        $writer->save('php://output');

        exit;

    }
}
