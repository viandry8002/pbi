<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Overview extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
		// cek user session
		is_logged_in();

		// load model
        $this->load->model('Global_Model');
        $this->load->model('InvestmentCostsDetail_Model');
        $this->load->model('ReductionCosts_Model');
        $this->load->model('AdditionalCosts_Model');
        $this->load->model('MaterialCosts_Model');
        $this->load->model('EmployeeCosts_Model');
        $this->load->model('ProductionCosts_Model');
        $this->load->model('LoanCosts_Model');
        $this->load->model('ProfitAnalysis_Model');
    }

    // function index overview
	public function index()
	{
		// set data
		$data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
		$data['title'] = 'Dashboard';

		// sub total pengadaan lahan
        $data['stpengadaan_lahan'] = $this->Global_Model->pengadaanLahan();
        // sub total bangunan
        $data['stbangunan'] = $this->Global_Model->bangunan();
        // sub total mesin dan peralatan
        $data['stmesin'] = $this->Global_Model->mesinPeralatan();
        // sub total fasilitsa
        $data['stfasilitas'] = $this->Global_Model->fasilitas();
        // sub total kendaraan
        $data['stkendaraan'] = $this->Global_Model->kendaraan();
        // sub total pra investasi
        $data['stprainvest'] = $this->Global_Model->prainvestasi();
        // total investasi
        $data['totalinvestasi'] = $this->Global_Model->totalInvestasi($data['stpengadaan_lahan'], $data['stbangunan'], $data['stmesin'], $data['stfasilitas'], $data['stkendaraan'], $data['stprainvest']);
        // get total modal kerja
        $data['modalkerja'] = $this->Global_Model->modalKerja();
        // total kotingensi
        $data['kotingensi'] = $this->Global_Model->kotingensi($data['totalinvestasi'], $data['modalkerja']['subtotal']);
		// total biaya investasi
		$data['totalbaiayainvest'] = $this->Global_Model->totalBiayaInvest($data['totalinvestasi'], $data['modalkerja']['subtotal'], $data['kotingensi']);

		// total biaya penyusutan
		$data['stpsth'] = $this->Global_Model->biayaPenyusutanPsth();

		// sub total pemeliharaan
        $data['stpemeliharaan'] = $this->Global_Model->pemeliharaan();
        // sub total asuransi
        $data['stasuransi'] = $this->Global_Model->asuransi();
        // get biaya administrasi
        $data['biayaadministrasi'] = $this->db->get_where('additional_costs', ['category_id' => 10])->row_array();
        // get biaya pajak bumi bangunan
        $data['biayapajakinvestasi'] = $this->db->get_where('additional_costs', ['sub_category_id' => 2])->row_array(); // biaya investasi
        $data['biayapajaktahunan'] = $this->Global_Model->pajakBumi($data['biayapajakinvestasi']['investment_costs']);
        // get biaya pemasaran
        $data['pemasaran'] = $this->db->get_where('additional_costs', ['category_id' => 11])->row_array();
        // total biaya lain lain
        $data['totalbiayalain'] = $this->Global_Model->totalBiayaLain($data['stpemeliharaan'], $data['stasuransi'], $data['biayaadministrasi']['costs_year'], $data['biayapajaktahunan'], $data['pemasaran']['costs_year']);

		// subtotal bahan baku
        $data['stbahanbaku'] = $this->Global_Model->biayaBahanBaku();

		// total biaya tenaga kerja
		// get sub total tk tidak langsung
        $data['sttktidaklangsung'] = $this->Global_Model->tkTidakLangsung();
		// get sub total tk langsung
        $data['sttklangsung'] = $this->Global_Model->tkLangsung();
		// get sub total biaya tk
        $data['sttk'] = $this->Global_Model->StTenagaKerja($data['sttktidaklangsung'], $data['sttklangsung']);

		// total biaya produksi
		// BIAYA TETAP
        // sub total tk.tidak langsung
        $data['tktidaklangsung'] = $this->Global_Model->tkTidakLangsung();
        // sub total pemeliharaan
        $data['pemeliharaan'] = $this->Global_Model->pemeliharaan();
        // sub total asuransi
        $data['asuransi'] = $this->Global_Model->asuransi();
        // sub total pbb
        $data['pajakbumi'] = $this->Global_Model->subPBB();
        // pemasaran
        $data['pemasaran'] = $this->db->get_where('additional_costs', ['category_id' => 11])->row_array();
        // administrasi
        $data['administrasi'] = $this->db->get_where('additional_costs', ['category_id' => 10])->row_array();
        // BIAYA TIDAk TETAP
        // sub total bahan baku dan input
        $data['bakuinput'] = $this->Global_Model->biayaBahanBaku();
        // sub total tk langsung
        $data['tklangsung'] = $this->Global_Model->tkLangsung();
        // sub total tk langsung tahun 1
        $data['tklangsung1'] = $data['tklangsung'] * 0.7;
        // sub total tk langsung tahun 2
        $data['tklangsung2'] = $data['tklangsung'] * 0.8;
        // get sub total biaya tetap
        $data['totalbiayatetap'] = $this->Global_Model->totalBiayaTetap($data['tktidaklangsung'], $data['pemeliharaan'], $data['asuransi'], $data['pajakbumi'], $data['pemasaran']['costs_year'], $data['administrasi']['costs_year']);
        // sub total biaya tidak tetap tahun 1
        $data['totalbiayatidaktetap1'] = $this->Global_Model->totalBiayaTidakTetap1($data['bakuinput'], $data['tklangsung1']);
        // sub total biaya tidak tetap tahun 2
        $data['totalbiayatidaktetap2'] = $this->Global_Model->totalBiayaTidakTetap2($data['bakuinput'], $data['tklangsung2']);
        // sub total biaya tidak tetap tahun 3
        $data['totalbiayatidaktetap3'] = $this->Global_Model->totalBiayaTidakTetap3($data['bakuinput'], $data['tklangsung']);
        // total biaya produksi 1
        $data['totalBP1'] = $this->Global_Model->totalBP1($data['totalbiayatetap'], $data['totalbiayatidaktetap1']);

		// sub total kewajiban pinjaman tahun 0
		$data['kewajibanpinjaman0'] = $this->db->get_where('loan_costs', ['year_of_loan' => 0])->row_array();

		$data['totalinvest'] = $this->InvestmentCostsDetail_Model->TotalInvestCosts();
		$data['toalreduction'] = $this->ReductionCosts_Model->TotalReductionCosts();
		$data['totaladditional'] = $this->AdditionalCosts_Model->TotalAdditionalCosts();
		$data['totalmaterial'] = $this->MaterialCosts_Model->TotalMaterialCosts();
		$data['totalemployee'] = $this->EmployeeCosts_Model->TotalEmployeeCosts();
		$data['totalproduction'] = $this->ProductionCosts_Model->TotalProductionCosts();
		$data['totalproduction'] = $this->ProductionCosts_Model->TotalProductionCosts();
		$data['totalloan'] = $this->LoanCosts_Model->TotalLoanCosts();
		$data['totalanalysis'] = $this->ProfitAnalysis_Model->TotalAnalysisCosts();
		
        // get data analisis rugi laba / laba bersih
		$data['analisis'] = $this->Global_Model->analisisFinansial();
		$data['lababersih'] = $data['analisis']['lb1'];
		// load view
		$this->load->view('templates/overview_header', $data);
		$this->load->view('templates/overview_sidebar');
		$this->load->view('templates/overview_topbar', $data);
		$this->load->view('overview/index', $data);
		$this->load->view('templates/overview_footer');
	}
}
