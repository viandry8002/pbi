<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoanCosts extends CI_Controller {

	// consturct
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        // load model
        $this->load->model('LoanCosts_Model');
        $this->load->model('Configuration_Model');
	}
	
    // function index loancosts
	public function index()
	{
		// set data
		$data['title'] = 'Kewajiban Pinjaman';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
		$data['loancosts'] = $this->LoanCosts_Model->getAll();
        $data['configuration'] = $this->db->get_where('configuration', ['id' => 1])->row_array();
		// load view
		$this->load->view('templates/overview_header', $data);
		$this->load->view('templates/overview_sidebar');
		$this->load->view('templates/overview_topbar', $data);
		$this->load->view('loancosts/index', $data);
		$this->load->view('templates/overview_footer');
	}

	// add loancosts
    public function add()
    {
		// validation
        $this->form_validation->set_rules('year_of_loan', 'Tahun', 'required', [
            'required' => 'Tahun harus di isi!'
		]);

        if ($this->form_validation->run() == false) {
            // load view
            $this->load->view('templates/overview_header', $data);
            $this->load->view('templates/overview_sidebar');
            $this->load->view('templates/overview_topbar');
            $this->load->view('loancosts/index', $data);
            $this->load->view('templates/overview_footer');
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
			Gagal Menambahkan Data!</div>');
            redirect('LoanCosts');
        } else {
            // get data configuration
            $data['configuration'] = $this->db->get_where('configuration', ['id' => 1])->row_array();
            // var total year 0
            $data['year0'] = $this->db->get_where('loan_costs', ['year_of_loan' => 0])->row_array();
            $year0 = $data['year0']['total'];
            // var total year1
            $data['total_year1'] = $this->db->get_where('loan_costs', ['year_of_loan' => 1])->row_array();
            $year1 = $data['total_year1']['total'];
            $instalment1 = $data['total_year1']['instalment'];
            // var total year2
            $data['total_year2'] = $this->db->get_where('loan_costs', ['year_of_loan' => 2])->row_array();
            $year2 = $data['total_year2']['total'];
            $instalment2 = $data['total_year2']['instalment'];
            // var total year3
            $data['total_year3'] = $this->db->get_where('loan_costs', ['year_of_loan' => 3])->row_array();
            $year3 = $data['total_year3']['total'];
            $instalment3 = $data['total_year3']['instalment'];
            // var total year4
            $data['total_year4'] = $this->db->get_where('loan_costs', ['year_of_loan' => 4])->row_array();
            $year4 = $data['total_year4']['total'];
            $instalment4 = $data['total_year4']['instalment'];
            // var total year5
            $data['total_year5'] = $this->db->get_where('loan_costs', ['year_of_loan' => 5])->row_array();
            $year5 = $data['total_year5']['total'];
            $instalment5 = $data['total_year5']['instalment'];
            // var total year6
            $data['total_year6'] = $this->db->get_where('loan_costs', ['year_of_loan' => 6])->row_array();
            $year6 = $data['total_year6']['total'];
            $instalment6 = $data['total_year6']['instalment'];
            // var total year7
            $data['total_year7'] = $this->db->get_where('loan_costs', ['year_of_loan' => 7])->row_array();
            $year7 = $data['total_year7']['total'];
            $instalment7 = $data['total_year7']['instalment'];
            // var total year8
            $data['total_year8'] = $this->db->get_where('loan_costs', ['year_of_loan' => 8])->row_array();
            $year8 = $data['total_year8']['total'];
            $instalment8 = $data['total_year8']['instalment'];
            // var total year9
            $data['total_year9'] = $this->db->get_where('loan_costs', ['year_of_loan' => 9])->row_array();
            $year9 = $data['total_year9']['total'];
            $instalment9 = $data['total_year9']['instalment'];
            // var interest loan
            $interestloan = $data['configuration']['interest_loan_costs'];
            // var post
            $post = $this->input->post();
            // set time zone
            date_default_timezone_set("Asia/Jakarta");
            // var year
            if ($post['year_of_loan'] == 0) {
                $total = $post['total'];
                $instalment = 0;
                $interest = 0;
                $totalinstalment = 0;
            } else if ($post['year_of_loan'] == 1) {
                $total = $year0;
                // var instalment
                $instalment = $year0 * 0.1;
                // var interest
                $interest = $total * ($interestloan / 100);
                // var total instalment
                $totalinstalment = $instalment + $interest; 
            } else if ($post['year_of_loan'] == 2) {
                $total = $year1 - $instalment1;
                $instalment = $year0 * 0.1;
                $interest = $total * ($interestloan / 100);
                $totalinstalment = $instalment + $interest; 
            }  else if ($post['year_of_loan'] == 3) {
                $total = $year2 - $instalment2;
                $instalment = $year0 * 0.1;
                $interest = $total * ($interestloan / 100);
                $totalinstalment = $instalment + $interest; 
            } else if ($post['year_of_loan'] == 4) {
                $total = $year3 - $instalment3;
                $instalment = $year0 * 0.1;
                $interest = $total * ($interestloan / 100);
                $totalinstalment = $instalment + $interest; 
            } else if ($post['year_of_loan'] == 5) {
                $total = $year4 - $instalment4;
                $instalment = $year0 * 0.1;
                $interest = $total * ($interestloan / 100);
                $totalinstalment = $instalment + $interest; 
            } else if ($post['year_of_loan'] == 6) {
                $total = $year5 - $instalment5;
                $instalment = $year0 * 0.1;
                $interest = $total * ($interestloan / 100);
                $totalinstalment = $instalment + $interest; 
            } else if ($post['year_of_loan'] == 7) {
                $total = $year6 - $instalment6;
                $instalment = $year0 * 0.1;
                $interest = $total * ($interestloan / 100);
                $totalinstalment = $instalment + $interest; 
            } else if ($post['year_of_loan'] == 8) {
                $total = $year7 - $instalment7;
                $instalment = $year0 * 0.1;
                $interest = $total * ($interestloan / 100);
                $totalinstalment = $instalment + $interest; 
            } else if ($post['year_of_loan'] == 9) {
                $total = $year8 - $instalment8;
                $instalment = $year0 * 0.1;
                $interest = $total * ($interestloan / 100);
                $totalinstalment = $instalment + $interest; 
            } else if ($post['year_of_loan'] == 10) {
                $total = $year9 - $instalment9;
                $instalment = $year0 * 0.1;
                $interest = $total * ($interestloan / 100);
                $totalinstalment = $instalment + $interest; 
            }
            // data array
            $data = [
                'year_of_loan' => $post['year_of_loan'],
                'total' => $total,
                'instalment' => $instalment,
                'interest' => $interest,
                'total_instalment' => $totalinstalment,
                'created_at' => date("y-m-d h:i:sa"),
                'updated_at' => date("y-m-d h:i:sa"),
            ];
            // call model
			$this->LoanCosts_Model->save($data);
            // set session
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Berhasil Menambahkan Data!</div>');
            // redirect
            redirect('LoanCosts');
		}
	}

	// edit loancosts
    public function edit($id = null)
    {
        // set data
        $data['title'] = "Ubah Kewajiban Pinjaman";
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $loancosts = $this->LoanCosts_Model;
        $data['loancosts'] = $loancosts->getById($id);
        $data['investmentcosts'] = $this->db->get('investment_costs')->result_array();
        
        // validation id
        if (!isset($id)) redirect('LoanCosts');
        if (!$data['loancosts']) show_404();

        // validation
        $this->form_validation->set_rules('year_of_loan', 'Tahun', 'required', [
            'required' => 'Tahun harus di isi!'
		]);

        if ($this->form_validation->run() == false) {
            // load view
            $this->load->view('templates/overview_header', $data);
            $this->load->view('templates/overview_sidebar');
            $this->load->view('templates/overview_topbar');
            $this->load->view('loancosts/edit_loancosts', $data);
            $this->load->view('templates/overview_footer');
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Data Gagal Diubah!</div>');
        } else {
            // get data configuration
            $data['configuration'] = $this->db->get_where('configuration', ['id' => 1])->row_array();
            // var total year 0
            $data['year0'] = $this->db->get_where('loan_costs', ['year_of_loan' => 0])->row_array();
            $year0 = $data['year0']['total'];
            // var total year1
            $data['total_year1'] = $this->db->get_where('loan_costs', ['year_of_loan' => 1])->row_array();
            $year1 = $data['total_year1']['total'];
            $instalment1 = $data['total_year1']['instalment'];
            // var total year2
            $data['total_year2'] = $this->db->get_where('loan_costs', ['year_of_loan' => 2])->row_array();
            $year2 = $data['total_year2']['total'];
            $instalment2 = $data['total_year2']['instalment'];
            // var total year3
            $data['total_year3'] = $this->db->get_where('loan_costs', ['year_of_loan' => 3])->row_array();
            $year3 = $data['total_year3']['total'];
            $instalment3 = $data['total_year3']['instalment'];
            // var total year4
            $data['total_year4'] = $this->db->get_where('loan_costs', ['year_of_loan' => 4])->row_array();
            $year4 = $data['total_year4']['total'];
            $instalment4 = $data['total_year4']['instalment'];
            // var total year5
            $data['total_year5'] = $this->db->get_where('loan_costs', ['year_of_loan' => 5])->row_array();
            $year5 = $data['total_year5']['total'];
            $instalment5 = $data['total_year5']['instalment'];
            // var total year6
            $data['total_year6'] = $this->db->get_where('loan_costs', ['year_of_loan' => 6])->row_array();
            $year6 = $data['total_year6']['total'];
            $instalment6 = $data['total_year6']['instalment'];
            // var total year7
            $data['total_year7'] = $this->db->get_where('loan_costs', ['year_of_loan' => 7])->row_array();
            $year7 = $data['total_year7']['total'];
            $instalment7 = $data['total_year7']['instalment'];
            // var total year8
            $data['total_year8'] = $this->db->get_where('loan_costs', ['year_of_loan' => 8])->row_array();
            $year8 = $data['total_year8']['total'];
            $instalment8 = $data['total_year8']['instalment'];
            // var total year9
            $data['total_year9'] = $this->db->get_where('loan_costs', ['year_of_loan' => 9])->row_array();
            $year9 = $data['total_year9']['total'];
            $instalment9 = $data['total_year9']['instalment'];
            // var interest loan
            $interestloan = $data['configuration']['interest_loan_costs'];
            // var post
            $post = $this->input->post();
            // set time zone
            date_default_timezone_set("Asia/Jakarta");
            // var year
            if ($post['year_of_loan'] == 0) {
                $total = $post['total'];
                $instalment = 0;
                $interest = 0;
                $totalinstalment = 0;
            } else if ($post['year_of_loan'] == 1) {
                $total = $year0;
                // var instalment
                $instalment = $year0 * 0.1;
                // var interest
                $interest = $total * ($interestloan / 100);
                // var total instalment
                $totalinstalment = $instalment + $interest; 
            } else if ($post['year_of_loan'] == 2) {
                $total = $year1 - $instalment1;
                $instalment = $year0 * 0.1;
                $interest = $total * ($interestloan / 100);
                $totalinstalment = $instalment + $interest; 
            }  else if ($post['year_of_loan'] == 3) {
                $total = $year2 - $instalment2;
                $instalment = $year0 * 0.1;
                $interest = $total * ($interestloan / 100);
                $totalinstalment = $instalment + $interest; 
            } else if ($post['year_of_loan'] == 4) {
                $total = $year3 - $instalment3;
                $instalment = $year0 * 0.1;
                $interest = $total * ($interestloan / 100);
                $totalinstalment = $instalment + $interest; 
            } else if ($post['year_of_loan'] == 5) {
                $total = $year4 - $instalment4;
                $instalment = $year0 * 0.1;
                $interest = $total * ($interestloan / 100);
                $totalinstalment = $instalment + $interest; 
            } else if ($post['year_of_loan'] == 6) {
                $total = $year5 - $instalment5;
                $instalment = $year0 * 0.1;
                $interest = $total * ($interestloan / 100);
                $totalinstalment = $instalment + $interest; 
            } else if ($post['year_of_loan'] == 7) {
                $total = $year6 - $instalment6;
                $instalment = $year0 * 0.1;
                $interest = $total * ($interestloan / 100);
                $totalinstalment = $instalment + $interest; 
            } else if ($post['year_of_loan'] == 8) {
                $total = $year7 - $instalment7;
                $instalment = $year0 * 0.1;
                $interest = $total * ($interestloan / 100);
                $totalinstalment = $instalment + $interest; 
            } else if ($post['year_of_loan'] == 9) {
                $total = $year8 - $instalment8;
                $instalment = $year0 * 0.1;
                $interest = $total * ($interestloan / 100);
                $totalinstalment = $instalment + $interest; 
            } else if ($post['year_of_loan'] == 10) {
                $total = $year9 - $instalment9;
                $instalment = $year0 * 0.1;
                $interest = $total * ($interestloan / 100);
                $totalinstalment = $instalment + $interest; 
            }
            // data array
            $data = [
                'id' => $post['id'],
                'year_of_loan' => $post['year_of_loan'],
                'total' => $total,
                'instalment' => $instalment,
                'interest' => $interest,
                'total_instalment' => $totalinstalment,
                'created_at' => $post['created_at'],
                'updated_at' => date("y-m-d h:i:sa"),
            ];

            // var di
            $id = $post['id'];

            // call model
			$this->LoanCosts_Model->update($data, $id);
            // set session
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Berhasil Mengubah Data!</div>');
            // redirect
            redirect('LoanCosts');
        }
	}
	
	// delete loan costs
    public function delete($id = null)
    {
        if (!isset($id)) show_404();
        $loancosts = $this->LoanCosts_Model;
        if ($loancosts->delete($id)) {
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data Berhasil Di Hapus!</div>');
            redirect('LoanCosts');
        }
    }

    // export to excel
    public function excel()
    {
        $data['loancosts'] = $this->LoanCosts_Model->getAll();
        include APPPATH.'PHPExcel-1.8/Classes/PHPExcel.php';
        include APPPATH.'PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php';

        $object = new PHPExcel();
        $object->getProperties()->setCreator("Kewajiban Pinjaman");
        $object->getProperties()->setLastModifiedBy("Kewajiban Pinjaman");
        $object->getProperties()->setTitle("Kewajiban Pinjaman");

        $object->setActiveSheetIndex(0);

        $object->getActiveSheet()->setCellValue('A1', 'NO');
        $object->getActiveSheet()->setCellValue('B1', 'Tahun Pinjaman');
        $object->getActiveSheet()->setCellValue('C1', 'Total');
        $object->getActiveSheet()->setCellValue('D1', 'Angsuran');
        $object->getActiveSheet()->setCellValue('E1', 'Bunga 22%');
        $object->getActiveSheet()->setCellValue('F1', 'Total Cicilan');

        $baris = 2;
        $no = 1;

        foreach ($data['loancosts'] as $icd) {
            $object->getActiveSheet()->setCellValue('A'.$baris, $no++);
            $object->getActiveSheet()->setCellValue('B'.$baris, $icd['year_of_loan']);
            $object->getActiveSheet()->setCellValue('C'.$baris, $icd['total']);
            $object->getActiveSheet()->setCellValue('D'.$baris, $icd['instalment']);
            $object->getActiveSheet()->setCellValue('E'.$baris, $icd['interest']);
            $object->getActiveSheet()->setCellValue('F'.$baris, $icd['total_instalment']);

            $baris++;
        }

        $filename = "Kewajiban_Pinjaman".'.xlsx';

        $object->getActiveSheet()->setTitle("Kewajiban Pinjaman");

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'. $filename .'"'); 
		header('Cache-Control: max-age=0');

        $writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        $writer->save('php://output');

        exit;

    }
    
}
