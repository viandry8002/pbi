<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class InvestmentCosts extends CI_Controller {

    // consturct
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        // load model
        $this->load->model('InvestmentCosts_Model');
    }

    // index investmentcosts
	public function index()
	{
		// set data
        $data['title'] = 'Perhitungan Investasi';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['investmentcosts'] = $this->db->order_by("id", "DESC");
        $data['investmentcosts'] = $this->InvestmentCosts_Model->getAll();
		// load view
		$this->load->view('templates/overview_header', $data);
		$this->load->view('templates/overview_sidebar');
		$this->load->view('templates/overview_topbar', $data);
		$this->load->view('investmentcosts/index', $data);
		$this->load->view('templates/overview_footer');
    }
    
    // add investmentcosts
    public function add()
    {
        // validation
        $this->form_validation->set_rules('title', 'Nama Perhitungan Investasi', 'required', [
            'required' => 'Nama Perhitungan Investasi harus di isi!'
        ]);

        if ($this->form_validation->run() == false) {
            // load view
            $this->load->view('templates/overview_header', $data);
            $this->load->view('templates/overview_sidebar');
            $this->load->view('templates/overview_topbar');
            $this->load->view('investmentcosts/index', $data);
            $this->load->view('templates/overview_footer');
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Gagal Menambahkan Data!</div>');
            redirect('InvestmentCosts');
        } else {
            $this->InvestmentCosts_Model->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Berhasil Menambahkan Data!</div>');
            redirect('InvestmentCosts');
        }
    }

    // edit investmentcosts
    public function edit($id = null)
    {
        // set data
        $data['title'] = "Ubah Perhitungan Investasi";
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $investmentcosts = $this->InvestmentCosts_Model;
        $data['investmentcosts'] = $investmentcosts->getById($id);
        // validation id
        if (!isset($id)) redirect('InvestmentCosts');
        if (!$data['investmentcosts']) show_404();
        // validation
        $this->form_validation->set_rules('title', 'Nama Perhitungan Investasi', 'required', [
            'required' => 'Nama Perhitungan Investasi harus di isi!'
        ]);

        if ($this->form_validation->run() == false) {
            // load view
            $this->load->view('templates/overview_header', $data);
            $this->load->view('templates/overview_sidebar');
            $this->load->view('templates/overview_topbar');
            $this->load->view('investmentcosts/edit_investmentcosts', $data);
            $this->load->view('templates/overview_footer');
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Data Gagal Diubah!</div>');
        } else {
            $investmentcosts->update();
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data Berhasil Diubah!</div>');
            redirect('InvestmentCosts');
        }
    }

    // delete investmentcosts
    public function delete($id = null)
    {
        if (!isset($id)) show_404();
        $investmentcosts = $this->InvestmentCosts_Model;
        if ($investmentcosts->delete($id)) {
            // session delete data
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data Berhasil Di Hapus!</div>');
            redirect('InvestmentCosts');
        }
    }
}
