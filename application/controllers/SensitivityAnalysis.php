<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SensitivityAnalysis extends CI_Controller {

	// consturct
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
	}

    // function index
    public function index()
    {
        // set data
        $data['title'] = 'Analisis Sensitifitas';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
		// load view
		$this->load->view('templates/overview_header', $data);
		$this->load->view('templates/overview_sidebar');
		$this->load->view('templates/overview_topbar', $data);
		$this->load->view('sensitivityanalysis/index', $data);
		$this->load->view('templates/overview_footer');
    }
}