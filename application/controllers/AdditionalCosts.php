<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdditionalCosts extends CI_Controller {

	// consturct
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        // load model
        $this->load->model('AdditionalCosts_Model');
        $this->load->model('Global_Model');
	}
	
    // function index additionalcosts
	public function index()
	{
        // sub total pemeliharaan
        $data['stpemeliharaan'] = $this->Global_Model->pemeliharaan();
        // sub total asuransi
        $data['stasuransi'] = $this->Global_Model->asuransi();
        // get biaya administrasi
        $data['biayaadministrasi'] = $this->db->get_where('additional_costs', ['category_id' => 10])->row_array();
        // get biaya pajak bumi bangunan
        $data['biayapajakinvestasi'] = $this->db->get_where('additional_costs', ['sub_category_id' => 2])->row_array(); // biaya investasi
        $data['biayapajaktahunan'] = $this->Global_Model->pajakBumi($data['biayapajakinvestasi']['investment_costs']);
        // get biaya pemasaran
        $data['pemasaran'] = $this->db->get_where('additional_costs', ['category_id' => 11])->row_array();

        // total biaya lain lain
        $data['totalbiayalain'] = $this->Global_Model->totalBiayaLain($data['stpemeliharaan'], $data['stasuransi'], $data['biayaadministrasi']['costs_year'], $data['biayapajaktahunan'], $data['pemasaran']['costs_year']);

		// set data
		$data['title'] = 'Biaya Pemeliharaan, Asuransi, Administrasi, Pajak Bumi Bangunan, Pemasaran';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
		$data['additionalcosts'] = $this->AdditionalCosts_Model->getAll();
		$data['category_biayalain'] = $this->db->get_where('category', ['type' => 'biaya_lain'])->result_array();
		$data['category_baiayainvest'] = $this->db->get_where('category', ['type' => 'biaya_investasi',])->result_array();
		// load view
		$this->load->view('templates/overview_header', $data);
		$this->load->view('templates/overview_sidebar');
		$this->load->view('templates/overview_topbar', $data);
		$this->load->view('additionalcosts/index', $data);
		$this->load->view('templates/overview_footer');
	}

	// add additionalcosts
    public function add()
    {
        // get sub total bangunan
        $bangunan = $this->Global_Model->bangunan();
        // get sub total mesin
        $mesin = $this->Global_Model->mesinPeralatan();
        // get sub total fasilitas
        $fasilitas = $this->Global_Model->fasilitas();
        // sub total kendaraan
        $kendaraan = $this->Global_Model->kendaraan();

        // validation
        $this->form_validation->set_rules('category_id', 'Kategori', 'required', [
            'required' => 'Kategori harus di isi!'
        ]);

        if ($this->form_validation->run() == false) {
            // load view
            $this->load->view('templates/overview_header', $data);
            $this->load->view('templates/overview_sidebar');
            $this->load->view('templates/overview_topbar');
            $this->load->view('additionalcosts/index', $data);
            $this->load->view('templates/overview_footer');
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Gagal Menambahkan Data!</div>');
            redirect('AdditionalCosts');
        } else {
            // var post
			$post = $this->input->post();
            
            // logic costs year
            if ($post['category_id'] == 8 && $post['sub_category_id'] == 2) {
                $invetscosts = $bangunan;
                $costsyear = $bangunan * 0.02;
            } else if ($post['category_id'] == 8 && $post['sub_category_id'] == 3) {
                $invetscosts = $mesin;
                $costsyear = $mesin * 0.02;
            } else if ($post['category_id'] == 8 && $post['sub_category_id'] == 4) {
                $invetscosts = $fasilitas;
                $costsyear = $fasilitas * 0.02;
            } else if ($post['category_id'] == 8 && $post['sub_category_id'] == 5) {
                $invetscosts = $kendaraan;
                $costsyear = $kendaraan * 0.02;
            } else if ($post['category_id'] == 9 && $post['sub_category_id'] == 2) {
                $invetscosts = $bangunan;
                $costsyear = $bangunan * 0.1;
            } else if ($post['category_id'] == 9 && $post['sub_category_id'] == 3) {
                $invetscosts = $mesin;
                $costsyear = $mesin * 0.02;
            } else if ($post['category_id'] == 10) {
                $invetscosts = 0;
                $costsyear = 0;
            } else if ($post['category_id'] == 11) {
                $invetscosts = 0;
                $costsyear = 0;
            }

            // set date
            date_default_timezone_set("Asia/Jakarta");
            // data array
            $data = [
                'category_id' => $post['category_id'],
                'sub_category_id' => $post['sub_category_id'],
                'investment_costs' => $invetscosts,
                'costs_year' => $costsyear,
                'created_at' => date("y-m-d h:i:sa"),
                'updated_at' => date("y-m-d h:i:sa"),
            ];
            // call model
            $this->AdditionalCosts_Model->save($data);
            // set session
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Berhasil Menambahkan Data!</div>');
            // redirect
            redirect('AdditionalCosts');
        }
	}
	
	// function edit
    public function edit($id = null)
    {
        // get sub total bangunan
        $bangunan = $this->Global_Model->bangunan();
        // get sub total mesin
        $mesin = $this->Global_Model->mesinPeralatan();
        // get sub total fasilitas
        $fasilitas = $this->Global_Model->fasilitas();
        // sub total kendaraan
        $kendaraan = $this->Global_Model->kendaraan();

        // set data
        $data['title'] = "Ubah Biaya Pemeliharaan, Asuransi, Administrasi, Pajak Bumi Bangunan, Pemasaran";
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $additionalcosts = $this->AdditionalCosts_Model;
		$data['additionalcosts'] = $additionalcosts->getById($id);
		$data['category_biayalain'] = $this->db->get_where('category', ['type' => 'biaya_lain'])->result_array();
		$data['category_baiayainvest'] = $this->db->get_where('category', ['type' => 'biaya_investasi'])->result_array();
        // validation id
        if (!isset($id)) redirect('AdditionalCosts');
        if (!$data['additionalcosts']) show_404();
        // validation
        $this->form_validation->set_rules('category_id', 'kategori', 'required', [
            'required' => 'kategori harus di isi!'
        ]);

        if ($this->form_validation->run() == false) {
            // load view
            $this->load->view('templates/overview_header', $data);
            $this->load->view('templates/overview_sidebar');
            $this->load->view('templates/overview_topbar');
            $this->load->view('additionalcosts/edit_additionalcosts', $data);
            $this->load->view('templates/overview_footer');
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Data Gagal Diubah!</div>');
        } else {
            // var post
			$post = $this->input->post();
            // logic costs year
            if ($post['category_id'] == 8 && $post['sub_category_id'] == 2) {
                $invetscosts = $bangunan;
                $costsyear = $bangunan * 0.02;
            } else if ($post['category_id'] == 8 && $post['sub_category_id'] == 3) {
                $invetscosts = $mesin;
                $costsyear = $mesin * 0.02;
            } else if ($post['category_id'] == 8 && $post['sub_category_id'] == 4) {
                $invetscosts = $fasilitas;
                $costsyear = $fasilitas * 0.02;
            } else if ($post['category_id'] == 8 && $post['sub_category_id'] == 5) {
                $invetscosts = $kendaraan;
                $costsyear = $kendaraan * 0.02;
            } else if ($post['category_id'] == 9 && $post['sub_category_id'] == 2) {
                $invetscosts = $bangunan;
                $costsyear = $bangunan * 0.1;
            } else if ($post['category_id'] == 9 && $post['sub_category_id'] == 3) {
                $invetscosts = $mesin;
                $costsyear = $mesin * 0.02;
            }
            // set date
            date_default_timezone_set("Asia/Jakarta");
            // data array
            $data = [
                'id' => $post['id'],
                'category_id' => $post['category_id'],
                'sub_category_id' => $post['sub_category_id'],
                'investment_costs' => $invetscosts,
                'costs_year' => $costsyear,
                'created_at' => $post['created_at'],
                'updated_at' => date("y-m-d h:i:sa"),
            ];
            // var id
            $id = $post['id'];
            // call model
            $this->AdditionalCosts_Model->update($data, $id);
            // set session
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Berhasil Menambahkan Data!</div>');
            // redirect
            redirect('AdditionalCosts');
        }
	}

    // function edit biaya administrasi
    public function editbiayaadmin($id = null)
    {
        // set data
        $data['title'] = "Ubah Biaya Lain-lain";
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $additionalcosts = $this->AdditionalCosts_Model;
		$data['additionalcosts'] = $additionalcosts->getById($id);
        // validation id
        if (!isset($id)) redirect('AdditionalCosts');
        if (!$data['additionalcosts']) show_404();
        // validation
        $this->form_validation->set_rules('costs_year', 'Biaya administrasi', 'required', [
            'required' => 'kategori harus di isi!'
        ]);

        if ($this->form_validation->run() == false) {
            // load view
            $this->load->view('templates/overview_header', $data);
            $this->load->view('templates/overview_sidebar');
            $this->load->view('templates/overview_topbar');
            $this->load->view('additionalcosts/edit_biayaadmin', $data);
            $this->load->view('templates/overview_footer');
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Data Gagal Diubah!</div>');
        } else {
            // var post
			$post = $this->input->post();
            // set date
            date_default_timezone_set("Asia/Jakarta");
            // data array
            $data = [
                'id' => $post['id'],
                'category_id' => $post['category_id'],
                'sub_category_id' => $post['sub_category_id'],
                'investment_costs' => 0,
                'costs_year' => $post['costs_year'],
                'created_at' => $post['created_at'],
                'updated_at' => date("y-m-d h:i:sa"),
            ];
            // var id
            $id = $post['id'];
            // call model
            $this->AdditionalCosts_Model->update($data, $id);
            // set session
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Berhasil Menambahkan Data!</div>');
            // redirect
            redirect('AdditionalCosts');
        }
	}
	
	// function delete
	public function delete($id = null)
    {
        if (!isset($id)) show_404();
        $additionalcosts = $this->AdditionalCosts_Model;
        if ($additionalcosts->delete($id)) {
            // session delete data
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data Berhasil Di Hapus!</div>');
            redirect('AdditionalCosts');
        }
    }

    // export to excel
    public function excel()
    {
        $data['investmentcostsdetail'] = $this->AdditionalCosts_Model->getAll();
        include APPPATH.'PHPExcel-1.8/Classes/PHPExcel.php';
        include APPPATH.'PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php';

        $object = new PHPExcel();
        $object->getProperties()->setCreator("Biaya Lain");
        $object->getProperties()->setLastModifiedBy("Biaya Lain");
        $object->getProperties()->setTitle("Biaya Detail Lain");

        $object->setActiveSheetIndex(0);

        $object->getActiveSheet()->setCellValue('A1', 'NO');
        $object->getActiveSheet()->setCellValue('B1', 'Kategori');
        $object->getActiveSheet()->setCellValue('C1', 'Sub Kategori');
        $object->getActiveSheet()->setCellValue('D1', 'Biaya Investasi');
        $object->getActiveSheet()->setCellValue('E1', 'Biaya Pertahun');

        $baris = 2;
        $no = 1;

        foreach ($data['AdditionalCosts_Model'] as $icd) {
            $object->getActiveSheet()->setCellValue('A'.$baris, $no++);
            $object->getActiveSheet()->setCellValue('B'.$baris, $icd['title_c']);
            $object->getActiveSheet()->setCellValue('C'.$baris, $icd['title_cc']);
            $object->getActiveSheet()->setCellValue('D'.$baris, $icd['investment_costs']);
            $object->getActiveSheet()->setCellValue('E'.$baris, $icd['costs_year']);

            $baris++;
        }

        $filename = "Data_Biaya_Lain".'.xlsx';

        $object->getActiveSheet()->setTitle("Data Biaya Lain");

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'. $filename .'"'); 
		header('Cache-Control: max-age=0');

        $writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        $writer->save('php://output');

        exit;

    }
}
