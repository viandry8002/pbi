<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FinancialAnalysis extends CI_Controller {

	// consturct
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('Global_Model');
	}

    // function index
    public function index()
    {
        // set data
        $data['title'] = 'Analisis Finansial';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        // data
        $data['analisis'] = $this->Global_Model->analisisFinansial();
        // ROI
        $data['roi1'] = ($data['analisis']['lsp1'] / 12) / $data['analisis']['tkm0'] * 100;
        $data['roi2'] = ($data['analisis']['lsp2'] / 12) / $data['analisis']['tkm0'] * 100;
        $data['roi3'] = ($data['analisis']['lsp2'] / 12) / $data['analisis']['tkm0'] * 100;
        // PBP
        $data['pbp1'] = $data['analisis']['analisis']['nilai1'] / 12;
        $data['total_pbp1'] = $data['analisis']['tkm0'] / $data['pbp1'];
        $data['pbp2'] = $data['analisis']['analisis']['nilai2'] / 12;
        $data['total_pbp2'] = $data['analisis']['tkm0'] / $data['pbp2'];
        $data['pbp3'] = $data['analisis']['analisis']['nilai3'] / 12;
        $data['total_pbp3'] = $data['analisis']['tkm0'] / $data['pbp3'];
        // BEP rupiah
        $data['bep1'] = ($data['analisis']['analisis']['totalbiayatidaktetap1'] / $data['analisis']['analisis']['nilai1']) - 1;
        $data['total_bep1'] = $data['analisis']['analisis']['totalbiayatetap'] / $data['bep1'];
        $data['bep2'] = ($data['analisis']['analisis']['totalbiayatidaktetap2'] / $data['analisis']['analisis']['nilai2']) - 1;
        $data['total_bep2'] = $data['analisis']['analisis']['totalbiayatetap'] / $data['bep2'];
        $data['bep3'] = ($data['analisis']['analisis']['totalbiayatidaktetap3'] / $data['analisis']['analisis']['nilai3']) - 1;
        $data['total_bep3'] = $data['analisis']['analisis']['totalbiayatetap'] / $data['bep3'];

        // get value profit analysis
        $data['sql_profit'] = $this->db->get_where('profit_analysis', ['id' => 1])->row_array();
        // sql configuration
        $data['sql_config'] = $this->db->get_where('configuration', ['id' => 1])->row_array();

        // bep unit
        $data['bepunit1'] = $data['total_bep1'] / $data['sql_config']['acceptance_value'];
        $data['bepunit2'] = $data['total_bep2'] / $data['sql_config']['acceptance_value'];
        $data['bepunit3'] = $data['total_bep3'] / $data['sql_config']['acceptance_value'];

        // NPV
        $data['npv1'] = ($data['analisis']['lb1'] * $data['sql_config']['discount_rate']) / $data['analisis']['tkm0'];
        $data['npv2'] = ($data['analisis']['lb2'] * $data['sql_config']['discount_rate']) / $data['analisis']['tkm0'];
        $data['npv3'] = ($data['analisis']['lb3'] * $data['sql_config']['discount_rate']) / $data['analisis']['tkm0'];
        
        // echo $data['analisis']['analisis']['totalbiayatetap'];
        // echo $data['analisis']['analisis']['totalbiayatidaktetap1'];
        // echo $data['analisis']['analisis']['nilai1'];
        
		// load view
		$this->load->view('templates/overview_header', $data);
		$this->load->view('templates/overview_sidebar');
		$this->load->view('templates/overview_topbar', $data);
		$this->load->view('financialanalysis/index', $data);
		$this->load->view('templates/overview_footer');
    }
}