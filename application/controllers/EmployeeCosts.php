<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EmployeeCosts extends CI_Controller {

	// consturct
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        // load model
        $this->load->model('EmployeeCosts_Model');
        $this->load->model('SubCategory_Model');
        $this->load->model('Global_Model');
	}
	
    // function index employeecosts
	public function index()
	{
        // get sub total tk tidak langsung
        $data['sttktidaklangsung'] = $this->Global_Model->tkTidakLangsung();
        // get jumlah tk tidak langsung
        $data['jumlahtktidaklangsung'] = $this->Global_Model->jumlahTkTidakLangsung();
        // get sub total tk langsung
        $data['sttklangsung'] = $this->Global_Model->tkLangsung();
        // get jumlah tk langsung
        $data['jumlahtklangsung'] = $this->Global_Model->jumlahTkLangsung();
        // get sub total biaya tk
        $data['sttk'] = $this->Global_Model->StTenagaKerja($data['sttktidaklangsung'], $data['sttklangsung']);
		// set data
		$data['title'] = 'Biaya Tenaga Kerja';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
		$data['employeecosts'] = $this->EmployeeCosts_Model->getAll();
		$data['category'] = $this->db->get_where('category', ['type' => 'biaya_tenaga_kerja'])->result_array();
		$query = 'SELECT * FROM sub_category WHERE category_id IN (44, 45)';
		$data['subcategory'] = $this->db->query($query)->result_array();
		// load view
		$this->load->view('templates/overview_header', $data);
		$this->load->view('templates/overview_sidebar');
		$this->load->view('templates/overview_topbar', $data);
		$this->load->view('employeecosts/index', $data);
		$this->load->view('templates/overview_footer');
	}

    // function list sub category
    public function listSubCategory()
    {
        $id_category = $this->input->post('id_category');
        $subcategory = $this->SubCategory_Model->ViewByCategory($id_category);

        $lists = "<option value=''>Pilih Sub Kategori</option>";

        foreach($subcategory as $data) {
            $lists .= "<option value='".$data['id']."'>".$data['title']."</option>";
        }

        $callback = array('list_sub_category' => $lists);

        echo json_encode($callback);
    }

	// function add employeecosts
	public function add()
	{
		// validation
        $this->form_validation->set_rules('category_id', 'Kategori', 'required', [
            'required' => 'Kategori harus di isi!'
        ]);
        $this->form_validation->set_rules('sub_category_id', 'Sub Kategori', 'required', [
            'required' => 'Sub kategori harus di isi!'
        ]);
        $this->form_validation->set_rules('total', 'Sub Kategori', 'required', [
            'required' => 'Total harus di isi!'
        ]);
        $this->form_validation->set_rules('price_unit', 'Sub Kategori', 'required', [
            'required' => 'Harga satuan harus di isi!'
        ]);

        if ($this->form_validation->run() == false) {
            // load view
            $this->load->view('templates/overview_header', $data);
            $this->load->view('templates/overview_sidebar');
            $this->load->view('templates/overview_topbar');
            $this->load->view('employeecosts/index', $data);
            $this->load->view('templates/overview_footer');
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Gagal Menambahkan Data!</div>');
            redirect('EmployeeCosts');
        } else {
            // var post
            $post = $this->input->post();
            // set time zone
            date_default_timezone_set("Asia/Jakarta");
            // value costs year
            $costsyear = $post['total'] * $post['price_unit'] * 12;
            // data array
            $data = [
                'category_id' => $post['category_id'],
                'sub_category_id' => $post['sub_category_id'],
                'total' => $post['total'],
                'price_unit' => $post['price_unit'],
                'costs_year' => $costsyear,
                'created_at' => date("y-m-d h:i:sa"),
                'updated_at' => date("y-m-d h:i:sa"),
            ];
            // call model
			$this->EmployeeCosts_Model->save($data);
            // set session
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Berhasil Menambahkan Data!</div>');
            // redirect
            redirect('EmployeeCosts');
        }
	}

	// function edit
	public function edit($id = null)
    {
        // set data
        $data['title'] = "Ubah Biaya Tenaga Kerja";
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $employeecosts = $this->EmployeeCosts_Model;
        $data['employeecosts'] = $employeecosts->getById($id);
		$data['category'] = $this->db->get_where('category', ['type' => 'biaya_tenaga_kerja'])->result_array();
		$query = 'SELECT * FROM sub_category WHERE category_id IN (44, 45)';
		$data['subcategory'] = $this->db->query($query)->result_array();
        // validation id
        if (!isset($id)) redirect('EmployeeCosts');
        if (!$data['employeecosts']) show_404();
        // validation
        $this->form_validation->set_rules('category_id', 'Kategori', 'required', [
            'required' => 'Kategori harus di isi!'
        ]);
        $this->form_validation->set_rules('sub_category_id', 'Sub Kategori', 'required', [
            'required' => 'Sub kategori harus di isi!'
        ]);
        $this->form_validation->set_rules('total', 'Sub Kategori', 'required', [
            'required' => 'Total harus di isi!'
        ]);
        $this->form_validation->set_rules('price_unit', 'Sub Kategori', 'required', [
            'required' => 'Harga satuan harus di isi!'
        ]);

        if ($this->form_validation->run() == false) {
            // load view
            $this->load->view('templates/overview_header', $data);
            $this->load->view('templates/overview_sidebar');
            $this->load->view('templates/overview_topbar');
            $this->load->view('employeecosts/edit_employeecosts', $data);
            $this->load->view('templates/overview_footer');
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Data Gagal Diubah!</div>');
        } else {
            // var post
            $post = $this->input->post();
            // set time zone
            date_default_timezone_set("Asia/Jakarta");
            // value costs year
            $costsyear = $post['total'] * $post['price_unit'] * 12;
            // data array
            $data = [
                'id' => $post['id'],
                'category_id' => $post['category_id'],
                'sub_category_id' => $post['sub_category_id'],
                'total' => $post['total'],
                'price_unit' => $post['price_unit'],
                'costs_year' => $costsyear,
                'created_at' => $post['created_at'],
                'updated_at' => date("y-m-d h:i:sa"),
            ];
            // var id
            $id = $post['id'];
            // call model
            $employeecosts->update($data, $id);
            // set session
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data Berhasil Diubah!</div>');
            // redirect
            redirect('EmployeeCosts');
        }
	}
	
	// delete employeecosts
    public function delete($id = null)
    {
        if (!isset($id)) show_404();
        $employeecosts = $this->EmployeeCosts_Model;
        if ($employeecosts->delete($id)) {
            // session delete data
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data Berhasil Di Hapus!</div>');
            redirect('EmployeeCosts');
        }
    }

    // export to excel
    public function excel()
    {
        $data['employeecosts'] = $this->EmployeeCosts_Model->getAll();
        include APPPATH.'PHPExcel-1.8/Classes/PHPExcel.php';
        include APPPATH.'PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php';

        $object = new PHPExcel();
        $object->getProperties()->setCreator("Biaya Tenaga Kerja");
        $object->getProperties()->setLastModifiedBy("Biaya Tenaga Kerja");
        $object->getProperties()->setTitle("Biaya Detail Tenaga Kerja");

        $object->setActiveSheetIndex(0);

        $object->getActiveSheet()->setCellValue('A1', 'NO');
        $object->getActiveSheet()->setCellValue('B1', 'Kategori');
        $object->getActiveSheet()->setCellValue('C1', 'Sub Kategori');
        $object->getActiveSheet()->setCellValue('D1', 'Total / Jumlah');
        $object->getActiveSheet()->setCellValue('E1', 'Harga Satuan');
        $object->getActiveSheet()->setCellValue('F1', 'Biaya per Tahun');

        $baris = 2;
        $no = 1;

        foreach ($data['employeecosts'] as $ec) {
            $object->getActiveSheet()->setCellValue('A'.$baris, $no++);
            $object->getActiveSheet()->setCellValue('B'.$baris, $ec['title_c']);
            $object->getActiveSheet()->setCellValue('C'.$baris, $ec['title_sc']);
            $object->getActiveSheet()->setCellValue('D'.$baris, $ec['total']);
            $object->getActiveSheet()->setCellValue('E'.$baris, 'Rp.' . number_format($ec['price_unit']));
            $object->getActiveSheet()->setCellValue('F'.$baris, 'Rp.' . number_format($ec['costs_year']));

            $baris++;
        }

        $filename = "Data_Biaya_Tenaga_Kerja".'.xlsx';

        $object->getActiveSheet()->setTitle("Data Biaya Tenaga Kerja");

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'. $filename .'"'); 
		header('Cache-Control: max-age=0');

        $writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        $writer->save('php://output');

        exit;

    }
}
