<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CashExpensesIncome extends CI_Controller {

	// construct
	public function __construct()
	{
		parent::__construct();
        is_logged_in();
		$this->load->model('Global_Model');
		$this->load->model('CashExpensesIncome_Model');
	}

    // function index cashexpensesincome
	public function index()
	{
		$data['kas'] = $this->Global_Model->AnalisRugiLaba();
		$data['lababersih'] = $this->Global_Model->analisisFinansial();
		// laba bersin
		$data['lababersih']['lb1'];
		$data['lababersih']['lb2'];
		$data['lababersih']['lb3'];
        $data['kas']['lb1'];
        $data['kas']['lb2'];
        $data['kas']['lb3'];
        $data['kas']['lb4'];
        $data['kas']['lb2'];
        $data['kas']['lb5'];
        $data['kas']['lb6'];
        $data['kas']['lb7'];
        $data['kas']['lb8'];
        $data['kas']['lb9'];
        $data['kas']['lb10'];
		// penyusutan
		$data['penyusutan'] = $this->Global_Model->biayaPenyusutanPsth();
		// nilai sisa
		$data['nilaisisa'] = $this->db->get_where('reduction_costs', ['category_id' => 5])->row_array();
		$data['nilaisisa10'] = 92122000;
		// md.sendiri
		$data['mdsendiri'] = $this->db->get_where('cash_expenses_income', ['id' => 2])->row_array();
		// total kas masuk
		$mdpinjaman = $this->db->get_where('cash_expenses_income', ['sub_category_id' => 52])->row_array();
		$data['tkm0'] = $this->Global_Model->totalKasMasuk0($data['mdsendiri']['zero_year'], $mdpinjaman['zero_year']);
		$data['tkm1'] = $this->Global_Model->totalKasMasuk1($data['lababersih']['lb1'], $data['penyusutan']);
		$data['tkm2'] = $this->Global_Model->totalKasMasuk2($data['lababersih']['lb2'], $data['penyusutan']);
		$data['tkm3'] = $this->Global_Model->totalKasMasuk3($data['lababersih']['lb3'], $data['penyusutan']);
		$data['tkm4'] = $this->Global_Model->totalKasMasuk4($data['lababersih']['lb3'], $data['penyusutan']);
		$data['tkm5'] = $this->Global_Model->totalKasMasuk5($data['lababersih']['lb3'], $data['penyusutan'], $data['nilaisisa']['residual_value']);
		$data['tkm6'] = $this->Global_Model->totalKasMasuk6($data['lababersih']['lb3'], $data['penyusutan']);
		$data['tkm7'] = $this->Global_Model->totalKasMasuk7($data['lababersih']['lb3'], $data['penyusutan']);
		$data['tkm8'] = $this->Global_Model->totalKasMasuk8($data['lababersih']['lb3'], $data['penyusutan']);
		$data['tkm9'] = $this->Global_Model->totalKasMasuk9($data['lababersih']['lb3'], $data['penyusutan']);
		$data['tkm10'] = $this->Global_Model->totalKasMasuk10($data['lababersih']['lb3'], $data['penyusutan'], $data['nilaisisa10']);
        // biaya prduksi
        $data['kas']['totalBP1'];
        $data['kas']['totalBP2'];
        $data['kas']['totalBP3'];
        $data['kas']['totalBP3'];
        $data['kas']['totalBP3'];
        $data['kas']['totalBP3'];
        $data['kas']['totalBP3'];
        $data['kas']['totalBP3'];
        $data['kas']['totalBP3'];
        $data['kas']['totalBP3'];
        // biaya k.pinjaman
        $data['kp1'] = $this->Global_Model->kpinjaman1();
        $data['kp2'] = $this->Global_Model->kpinjaman2();
        $data['kp3'] = $this->Global_Model->kpinjaman3();
        $data['kp4'] = $this->Global_Model->kpinjaman4();
        $data['kp5'] = $this->Global_Model->kpinjaman5();
        $data['kp6'] = $this->Global_Model->kpinjaman6();
        $data['kp7'] = $this->Global_Model->kpinjaman7();
        $data['kp8'] = $this->Global_Model->kpinjaman8();
        $data['kp9'] = $this->Global_Model->kpinjaman9();
        $data['kp10'] = $this->Global_Model->kpinjaman10();
        // total kas keluar
        // yang bener
        $data['tkk1'] = $this->Global_Model->totalKasKeluar1($data['kas']['totalBP1'], $data['kp1']['total_instalment']);
        $data['tkk2'] = $this->Global_Model->totalKasKeluar2($data['kas']['totalBP2'], $data['kp2']['total_instalment']);
        $data['tkk3'] = $this->Global_Model->totalKasKeluar3($data['kas']['totalBP3'], $data['kp3']['total_instalment']);
        $data['tkk4'] = $this->Global_Model->totalKasKeluar4($data['kas']['totalBP3'], $data['kp4']['total_instalment']);
        $data['tkk5'] = $this->Global_Model->totalKasKeluar5($data['kas']['totalBP3'], $data['kp5']['total_instalment']);
        $data['tkk6'] = $this->Global_Model->totalKasKeluar6($data['kas']['totalBP3'], $data['kp6']['total_instalment']);
        $data['tkk7'] = $this->Global_Model->totalKasKeluar7($data['kas']['totalBP3'], $data['kp7']['total_instalment']);
        $data['tkk8'] = $this->Global_Model->totalKasKeluar8($data['kas']['totalBP3'], $data['kp8']['total_instalment']);
        $data['tkk9'] = $this->Global_Model->totalKasKeluar9($data['kas']['totalBP3'], $data['kp9']['total_instalment']);
        $data['tkk10'] = $this->Global_Model->totalKasKeluar10($data['kas']['totalBP3'], $data['kp10']['total_instalment']);
        // saldo kas
        $data['sk1'] = $this->Global_Model->saldoKas1($data['tkm1'], $data['tkk1']);
        $data['sk2'] = $this->Global_Model->saldoKas2($data['tkm2'], $data['tkk2']);
        $data['sk3'] = $this->Global_Model->saldoKas3($data['tkm3'], $data['tkk3']);
        $data['sk4'] = $this->Global_Model->saldoKas4($data['tkm4'], $data['tkk4']);
        $data['sk5'] = $this->Global_Model->saldoKas5($data['tkm5'], $data['tkk5']);
        $data['sk6'] = $this->Global_Model->saldoKas6($data['tkm6'], $data['tkk6']);
        $data['sk7'] = $this->Global_Model->saldoKas7($data['tkm7'], $data['tkk7']);
        $data['sk8'] = $this->Global_Model->saldoKas8($data['tkm8'], $data['tkk8']);
        $data['sk9'] = $this->Global_Model->saldoKas9($data['tkm9'], $data['tkk9']);
        $data['sk10'] = $this->Global_Model->saldoKas10($data['tkm10'], $data['tkk10']);
        // saldo kas komulatif tahun2-10
        $data['skk2'] = $this->Global_Model->saldoKomulatif2($data['sk1'], $data['sk2']);
        $data['skk3'] = $this->Global_Model->saldoKomulatif3($data['skk2'], $data['sk3']);
        $data['skk4'] = $this->Global_Model->saldoKomulatif4($data['skk3'], $data['sk4']);
        $data['skk5'] = $this->Global_Model->saldoKomulatif5($data['skk4'], $data['sk5']);
        $data['skk6'] = $this->Global_Model->saldoKomulatif6($data['skk5'], $data['sk6']);
        $data['skk7'] = $this->Global_Model->saldoKomulatif7($data['skk6'], $data['sk7']);
        $data['skk8'] = $this->Global_Model->saldoKomulatif8($data['skk7'], $data['sk8']);
        $data['skk9'] = $this->Global_Model->saldoKomulatif9($data['skk8'], $data['sk9']);
        $data['skk10'] = $this->Global_Model->saldoKomulatif10($data['skk9'], $data['sk10']);
		// set data
		$data['title'] = 'Arus Kas Pemasukan dan Pengeluaran';
		$data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
		$data['cashincome'] = $this->CashExpensesIncome_Model->getAll();
		// load view
		$this->load->view('templates/overview_header', $data);
		$this->load->view('templates/overview_sidebar');
		$this->load->view('templates/overview_topbar', $data);
		$this->load->view('cashexpensesincome/index', $data);
		$this->load->view('templates/overview_footer');
	}

	// function edit profit analysis
    public function edit($id = null)
    {
         // set data
        $data['title'] = "Ubah Nilai Modal";
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $cashincome = $this->CashExpensesIncome_Model;
        $data['cashincome'] = $cashincome->getById($id);
        
        // validation id
        if (!isset($id)) redirect('CashExpensesIncome');
        if (!$data['cashincome']) show_404();
        // validation
        $this->form_validation->set_rules('zero_year', 'Tahun 0', 'required', [
            'required' => 'Tahun 0 harus di isi!'
        ]);
        if ($this->form_validation->run() == false) {
            // load view
            $this->load->view('templates/overview_header', $data);
            $this->load->view('templates/overview_sidebar');
            $this->load->view('templates/overview_topbar', $data);
            $this->load->view('cashexpensesincome/edit_cashincome', $data);
            $this->load->view('templates/overview_footer');
        } else {
            // var post
            $post = $this->input->post();
            // set time zone
            date_default_timezone_set("Asia/Jakarta");
            // data array
            $data = [
                'id' => $post['id'],
                'category_id' => $post['category_id'],
                'sub_category_id' => $post['sub_category_id'],
				'zero_year' => $post['zero_year'],
                '1st_year' => $post['1st_year'],
                '2nd_year' => $post['2nd_year'],
                '3rd_year' => $post['3rd_year'],
                '4rd_year' => $post['4rd_year'],
                '5rd_year' => $post['5rd_year'],
                '6rd_year' => $post['6rd_year'],
                '7rd_year' => $post['7rd_year'],
                '8rd_year' => $post['8rd_year'],
                '9rd_year' => $post['9rd_year'],
                '10rd_year' => $post['10rd_year'],
                'created_at' => $post['created_at'],
                'updated_at' => date("y-m-d h:i:sa"),
            ];
            // var id
            $id = $post['id'];
            // call model
            $cashincome->update($data, $id);
            // set session
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data Berhasil Diubah!</div>');
            // redirect
            redirect('CashExpensesIncome');
        }
    }
}
