<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UnitData extends CI_Controller {

    // consturct
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        // load model
        $this->load->model('Unit_Model');
    }

    // index unit
	public function index()
	{
		// set data
        $data['title'] = 'Unit';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['unit'] = $this->db->order_by("id", "DESC");
        $data['unit'] = $this->Unit_Model->getAll();
		// load view
		$this->load->view('templates/overview_header', $data);
		$this->load->view('templates/overview_sidebar');
		$this->load->view('templates/overview_topbar', $data);
		$this->load->view('unitdata/index', $data);
		$this->load->view('templates/overview_footer');
    }
    
    // add unit
    public function add()
    {
        // validation
        $this->form_validation->set_rules('title', 'Nama Unit', 'required', [
            'required' => 'Nama Unit harus di isi!'
        ]);

        if ($this->form_validation->run() == false) {
            // load view
            $this->load->view('templates/overview_header', $data);
            $this->load->view('templates/overview_sidebar');
            $this->load->view('templates/overview_topbar');
            $this->load->view('unit/index', $data);
            $this->load->view('templates/overview_footer');
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Gagal Menambahkan Data!</div>');
            redirect('Unit');
        } else {
            $this->Unit_Model->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Berhasil Menambahkan Data!</div>');
            redirect('UnitData');
        }
    }

    // edit unit
    public function edit($id = null)
    {
        // set data
        $data['title'] = "Ubah Unit";
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $unit = $this->Unit_Model;
        $data['unit'] = $unit->getById($id);
        // validation id
        if (!isset($id)) redirect('Unit');
        if (!$data['unit']) show_404();
        // validation
        $this->form_validation->set_rules('title', 'Nama Unit', 'required', [
            'required' => 'Nama Unit harus di isi!'
        ]);

        if ($this->form_validation->run() == false) {
            // load view
            $this->load->view('templates/overview_header', $data);
            $this->load->view('templates/overview_sidebar');
            $this->load->view('templates/overview_topbar');
            $this->load->view('unitdata/edit_unit', $data);
            $this->load->view('templates/overview_footer');
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Data Gagal Diubah!</div>');
        } else {
            $unit->update();
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data Berhasil Diubah!</div>');
            redirect('UnitData');
        }
    }

    // delete unit
    public function delete($id = null)
    {
        if (!isset($id)) show_404();
        $unit = $this->Unit_Model;
        if ($unit->delete($id)) {
            // session delete data
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data Berhasil Di Hapus!</div>');
            redirect('UnitData');
        }
    }
}
