<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ReductionCostsData extends CI_Controller {

    // consturct
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        // load model
        $this->load->model('ReductionCosts_Model');
        $this->load->model('Global_Model');
    }

    // index reductioncosts
    public function index()
    {
        // sub total biaya penyusutan nbth
        $data['stnbth'] = $this->Global_Model->biayaPenyusutanNbth();
        // sub total psth
        $data['stpsth'] = $this->Global_Model->biayaPenyusutanPsth();
        // get sub total bangunan
        $data['bangunan'] = $this->Global_Model->bangunan();
        // get sub total mesin
        $data['mesin'] = $this->Global_Model->mesinPeralatan();
        // get sub total fasilitas
        $data['fasilitas'] = $this->Global_Model->fasilitas();
        // get sub total kendaraan
        $data['kendaraan'] = $this->Global_Model->kendaraan();

        // set data
        $data['title'] = 'Biaya Penyusutan';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['reductioncosts'] = $this->ReductionCosts_Model->getAll();
        $data['category'] = $this->db->get_where('category', ['type' => 'biaya_investasi'])->result_array();
        // load view
		$this->load->view('templates/overview_header', $data);
		$this->load->view('templates/overview_sidebar');
		$this->load->view('templates/overview_topbar', $data);
		$this->load->view('reductioncostsdata/index', $data);
		$this->load->view('templates/overview_footer');
    }

    // add reductioncosts
    public function add()
    {
        // get sub total bangunan
        $bangunan = $this->Global_Model->bangunan();
        // get sub total mesin
        $mesin = $this->Global_Model->mesinPeralatan();
        // get sub total fasilitas
        $fasilitas = $this->Global_Model->fasilitas();
        // get sub total kendaraan
        $kendaraan = $this->Global_Model->kendaraan();

        // validation
        $this->form_validation->set_rules('category_id', 'Kategori', 'required', [
            'required' => 'Kategori harus di isi!'
        ]);
        $this->form_validation->set_rules('ue_th', 'UE (th)', 'required', [
            'required' => 'UE (th) harus di isi!'
        ]);

        if ($this->form_validation->run() == false) {
            // load view
            $this->load->view('templates/overview_header', $data);
            $this->load->view('templates/overview_sidebar');
            $this->load->view('templates/overview_topbar');
            $this->load->view('reductioncosts/index', $data);
            $this->load->view('templates/overview_footer');
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Gagal Menambahkan Data!</div>');
            redirect('ReductionCosts');
        } else {
            // var post
            $post = $this->input->post();
            // post residual
            if ($post['category_id'] == 2) {
                $residual = $bangunan * 0.1;
                $psth = ($bangunan - $residual) / $post['ue_th'];
                $nbth = $residual * 5.5;
            } else if ($post['category_id'] == 3) {
                $residual = $mesin * 0.1;
                $psth = ($mesin - $residual) / $post['ue_th'];
                $nbth = $residual;
            } else if ($post['category_id'] == 4) {
                $residual = $fasilitas * 0.1;
                $psth = ($fasilitas - $residual) / $post['ue_th'];
                $nbth = $residual;
            } else if ($post['category_id'] == 5) {
                $residual = $kendaraan * 0.1;
                $psth = ($kendaraan - $residual) / $post['ue_th'];
                $nbth = $residual;
            }
            // set date time
            date_default_timezone_set("Asia/Jakarta");
            // data array
            $data = [
                'category_id' => $post['category_id'],
                'ue_th' => $post['ue_th'],
                'residual_value' => $residual,
                'nb_th_10' => $nbth,
                'created_at' =>date("y-m-d h:i:sa"),
                'updated_at' =>date("y-m-d h:i:sa"),
                'ps_th_10' => $psth
            ];
            // call model
            $this->ReductionCosts_Model->save($data);
            // set session
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Berhasil Menambahkan Data!</div>');
            // redirect
            redirect('ReductionCostsData');
        }
    }

    // edit reductioncosts
    public function edit($id = null)
    {
        // get sub total bangunan
        $bangunan = $this->Global_Model->bangunan();
        // get sub total mesin
        $mesin = $this->Global_Model->mesinPeralatan();
        // get sub total fasilitas
        $fasilitas = $this->Global_Model->fasilitas();
        // get sub total kendaraan
        $kendaraan = $this->Global_Model->kendaraan();
        // set data
        $data['title'] = "Ubah Biaya Penyusutan";
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $reductioncosts = $this->ReductionCosts_Model;
        $data['reductioncosts'] = $reductioncosts->getById($id);
        $data['category'] = $this->db->get_where('category', ['type' => 'biaya_investasi'])->result_array();
        // validation id
        if (!isset($id)) redirect('ReductionCosts');
        if (!$data['reductioncosts']) show_404();
        // validation
        $this->form_validation->set_rules('category_id', 'Kategori', 'required', [
            'required' => 'Kategori harus di isi!'
        ]);
        $this->form_validation->set_rules('ue_th', 'UE (th)', 'required', [
            'required' => 'UE (th) harus di isi!'
        ]);

        if ($this->form_validation->run() == false) {
            // load view
            $this->load->view('templates/overview_header', $data);
            $this->load->view('templates/overview_sidebar');
            $this->load->view('templates/overview_topbar');
            $this->load->view('reductioncostsdata/edit_reductioncosts', $data);
            $this->load->view('templates/overview_footer');
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Data Gagal Diubah!</div>');
        } else { 
            // var post
            $post = $this->input->post();

            // post residual
            if ($post['category_id'] == 2) {
                $residual = $bangunan * 0.1;
                $psth = ($bangunan - $residual) / $post['ue_th'];
                $nbth = $residual * 5.5;
            } else if ($post['category_id'] == 3) {
                $residual = $mesin * 0.1;
                $psth = ($mesin - $residual) / $post['ue_th'];
                $nbth = $residual;
            } else if ($post['category_id'] == 4) {
                $residual = $fasilitas * 0.1;
                $psth = ($fasilitas - $residual) / $post['ue_th'];
                $nbth = $residual;
            } else if ($post['category_id'] == 5) {
                $residual = $kendaraan * 0.1;
                $psth = ($kendaraan - $residual) / $post['ue_th'];
                $nbth = $residual;
            }
            // set date time
            date_default_timezone_set("Asia/Jakarta");
            // data array
            $data = [
                'id' => $post['id'],
                'category_id' => $post['category_id'],
                'ue_th' => $post['ue_th'],
                'residual_value' => $residual,
                'nb_th_10' => $nbth,
                'created_at' =>$post['created_at'],
                'updated_at' =>date("y-m-d h:i:sa"),
                'ps_th_10' => $psth
            ];
            // var id
            $id = $post['id'];
            // call model
            $this->ReductionCosts_Model->update($data, $id);
            // set session
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Berhasil Menambahkan Data!</div>');
            // redirect
            redirect('ReductionCostsData');
        }
    }

    // delete category
    public function delete($id = null)
    {
        if (!isset($id)) show_404();
        $reductioncosts = $this->ReductionCosts_Model;
        if ($reductioncosts->delete($id)) {
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data Berhasil Di Hapus!</div>');
            redirect('ReductionCostsData');
        }
    }

    // export to excel
    public function excel()
    {
        $data['reductioncosts'] = $this->ReductionCosts_Model->getAll();
        include APPPATH.'PHPExcel-1.8/Classes/PHPExcel.php';
        include APPPATH.'PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php';

        $object = new PHPExcel();
        $object->getProperties()->setCreator("Biaya Penyusutan");
        $object->getProperties()->setLastModifiedBy("Biaya Penyusutan");
        $object->getProperties()->setTitle("Biaya Penyusutan");

        $object->setActiveSheetIndex(0);

        $object->getActiveSheet()->setCellValue('A1', 'NO');
        $object->getActiveSheet()->setCellValue('B1', 'Kategori');
        $object->getActiveSheet()->setCellValue('C1', 'U.E(th)');
        $object->getActiveSheet()->setCellValue('D1', 'Nilai Sisa');
        $object->getActiveSheet()->setCellValue('E1', 'N.B th 10');

        $baris = 2;
        $no = 1;

        foreach ($data['reductioncosts'] as $rc) {
            $object->getActiveSheet()->setCellValue('A'.$baris, $no++);
            $object->getActiveSheet()->setCellValue('B'.$baris, $rc['title_c']);
            $object->getActiveSheet()->setCellValue('C'.$baris, $rc['ue_th']);
            $object->getActiveSheet()->setCellValue('D'.$baris, number_format($rc['residual_value']));
            $object->getActiveSheet()->setCellValue('E'.$baris, number_format($rc['nb_th_10']));

            $baris++;
        }

        $filename = "Data_Biaya_Penyusutan".'.xlsx';

        $object->getActiveSheet()->setTitle("Data Biaya Penyusutan");

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'. $filename .'"'); 
		header('Cache-Control: max-age=0');

        $writer = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
        $writer->save('php://output');

        exit;
    }
}