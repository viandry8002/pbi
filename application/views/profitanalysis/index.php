<script>
    function deleteConfirm(url){
        $('#btn-delete').attr('href', url);
        $('#deleteModal').modal();
    }
</script>

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
    <!--<a href="<?= base_url('ProfitAnalysis/excel'); ?>" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-2"><i class="fas fa-download fa-sm text-white-50"></i> Export Data</a>-->
    <div class="col-lg-7">
        <?= form_error('title', '<div class="alert alert-danger" role="alert">', '</div>'); ?>
        <?= $this->session->flashdata('message'); ?>
    </div>

    <div class="card col-lg-12 shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Laporan Analisis Rugi Laba</a></h6>
        </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="eksport" width="100%" cellspacing="0">
                        <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Kategori</th>
                                    <th>Sub Kategori</th>
                                    <th>Tahun 1</th>
                                    <th>Tahun 2</th>
                                    <th>Tahun 3</th>
                                    <th>Tahun 4</th>
                                    <th>Tahun 5</th>
                                    <th>Tahun 6</th>
                                    <th>Tahun 7</th>
                                    <th>Tahun 8</th>
                                    <th>Tahun 9</th>
                                    <th>Tahun 10</th>
                                </tr>
                        </thead>
                    <tbody>
                        <?php foreach($profitanalysis as $pa) : ?>
                        <tr>
                            <td>1</td>
                            <td>
                              <?= $pa['title_c'] ?>
                              <a class="badge badge-success" href="<?= site_url('ProfitAnalysis/edit/'.$pa['id']); ?>">Ubah</a>
                            </td>
                            <td><?= $pa['title_sc'] ?></td>
                            <td><?= number_format($pa['1st_year']) ?></td>
                            <td><?= number_format($pa['2nd_year']) ?></td>
                            <td><?= number_format($pa['3rd_year']) ?></td>
                            <td><?= number_format($pa['4rd_year']) ?></td>
                            <td><?= number_format($pa['5rd_year']) ?></td>
                            <td><?= number_format($pa['6rd_year']) ?></td>
                            <td><?= number_format($pa['7rd_year']) ?></td>
                            <td><?= number_format($pa['8rd_year']) ?></td>
                            <td><?= number_format($pa['9rd_year']) ?></td>
                            <td><?= number_format($pa['10rd_year']) ?></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td><?= $pa['title_c'] ?></td>
                            <td>
                                Nilai (<?= 'Rp.' . number_format($configuration['acceptance_value']) ?>)
                                <a class="badge badge-success" href="<?= site_url('ProfitAnalysis/editNilaiPenerimaan/'.$configuration['id']); ?>">Ubah</a>
                            </td>
                            <td><?= "Rp." . number_format($analisis['nilai1']) ?></td>
                            <td><?= "Rp." . number_format($analisis['nilai2']) ?></td>
                            <td><?= "Rp." . number_format($analisis['nilai3']) ?></td>
                            <td><?= "Rp." . number_format($analisis['nilai4']) ?></td>
                            <td><?= "Rp." . number_format($analisis['nilai5']) ?></td>
                            <td><?= "Rp." . number_format($analisis['nilai6']) ?></td>
                            <td><?= "Rp." . number_format($analisis['nilai7']) ?></td>
                            <td><?= "Rp." . number_format($analisis['nilai8']) ?></td>
                            <td><?= "Rp." . number_format($analisis['nilai9']) ?></td>
                            <td><?= "Rp." . number_format($analisis['nilai10']) ?></td>
                        </tr>
                        <tr>
                          <td>3</td>
                          <td><b>Total Penerimaan</b></td>
                          <td>-</td>
                          <td><?= "Rp." . number_format($analisis['nilai1']) ?></td>
                          <td><?= "Rp." . number_format($analisis['nilai2']) ?></td>
                          <td><?= "Rp." . number_format($analisis['nilai3']) ?></td>
                          <td><?= "Rp." . number_format($analisis['nilai4']) ?></td>
                          <td><?= "Rp." . number_format($analisis['nilai5']) ?></td>
                          <td><?= "Rp." . number_format($analisis['nilai6']) ?></td>
                          <td><?= "Rp." . number_format($analisis['nilai7']) ?></td>
                          <td><?= "Rp." . number_format($analisis['nilai8']) ?></td>
                          <td><?= "Rp." . number_format($analisis['nilai9']) ?></td>
                          <td><?= "Rp." . number_format($analisis['nilai10']) ?></td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Biaya Produksi</td>
                            <td>Biaya Tetap</td>
                            <td><?= "Rp." . number_format($analisis['totalbiayatetap']) ?></td>
                            <td><?= "Rp." . number_format($analisis['totalbiayatetap']) ?></td>
                            <td><?= "Rp." . number_format($analisis['totalbiayatetap']) ?></td>
                            <td><?= "Rp." . number_format($analisis['totalbiayatetap']) ?></td>
                            <td><?= "Rp." . number_format($analisis['totalbiayatetap']) ?></td>
                            <td><?= "Rp." . number_format($analisis['totalbiayatetap']) ?></td>
                            <td><?= "Rp." . number_format($analisis['totalbiayatetap']) ?></td>
                            <td><?= "Rp." . number_format($analisis['totalbiayatetap']) ?></td>
                            <td><?= "Rp." . number_format($analisis['totalbiayatetap']) ?></td>
                            <td><?= "Rp." . number_format($analisis['totalbiayatetap']) ?></td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>Biaya Produksi</td>
                            <td>Biaya Tidak Tetap</td>
                            <td><?= "Rp." . number_format($analisis['totalbiayatidaktetap1']) ?></td>
                            <td><?= "Rp." . number_format($analisis['totalbiayatidaktetap2']) ?></td>
                            <td><?= "Rp." . number_format($analisis['totalbiayatidaktetap3']) ?></td>
                            <td><?= "Rp." . number_format($analisis['totalbiayatidaktetap3']) ?></td>
                            <td><?= "Rp." . number_format($analisis['totalbiayatidaktetap3']) ?></td>
                            <td><?= "Rp." . number_format($analisis['totalbiayatidaktetap3']) ?></td>
                            <td><?= "Rp." . number_format($analisis['totalbiayatidaktetap3']) ?></td>
                            <td><?= "Rp." . number_format($analisis['totalbiayatidaktetap3']) ?></td>
                            <td><?= "Rp." . number_format($analisis['totalbiayatidaktetap3']) ?></td>
                            <td><?= "Rp." . number_format($analisis['totalbiayatidaktetap3']) ?></td>
                        </tr>
                        <tr>
                          <td>5</td>
                          <td><b>Total Biaya Produksi</b></td>
                          <td>-</td>
                          <td><?= "Rp." . number_format($analisis['totalBP1']) ?></td>
                          <td><?= "Rp." . number_format($analisis['totalBP2']) ?></td>
                          <td><?= "Rp." . number_format($analisis['totalBP3']) ?></td>
                          <td><?= "Rp." . number_format($analisis['totalBP3']) ?></td>
                          <td><?= "Rp." . number_format($analisis['totalBP3']) ?></td>
                          <td><?= "Rp." . number_format($analisis['totalBP3']) ?></td>
                          <td><?= "Rp." . number_format($analisis['totalBP3']) ?></td>
                          <td><?= "Rp." . number_format($analisis['totalBP3']) ?></td>
                          <td><?= "Rp." . number_format($analisis['totalBP3']) ?></td>
                          <td><?= "Rp." . number_format($analisis['totalBP3']) ?></td>
                        </tr>
                        <tr>
                          <td>5</td>
                          <td>Laba Operasi</td>
                          <td>-</td>
                          <td><?= "Rp." . number_format($lo1) ?></td>
                          <td><?= "Rp." . number_format($lo2) ?></td>
                          <td><?= "Rp." . number_format($lo3) ?></td>
                          <td><?= "Rp." . number_format($lo3) ?></td>
                          <td><?= "Rp." . number_format($lo3) ?></td>
                          <td><?= "Rp." . number_format($lo3) ?></td>
                          <td><?= "Rp." . number_format($lo3) ?></td>
                          <td><?= "Rp." . number_format($lo3) ?></td>
                          <td><?= "Rp." . number_format($lo3) ?></td>
                          <td><?= "Rp." . number_format($lo3) ?></td>
                        </tr>
                        <tr>
                          <td>6</td>
                          <td>Penyusutan</td>
                          <td>-</td>
                          <td><?= "Rp." . number_format($analisis['penyusutan1']) ?></td>
                          <td><?= "Rp." . number_format($analisis['penyusutan1']) ?></td>
                          <td><?= "Rp." . number_format($analisis['penyusutan1']) ?></td>
                          <td><?= "Rp." . number_format($analisis['penyusutan1']) ?></td>
                          <td><?= "Rp." . number_format($analisis['penyusutan1']) ?></td>
                          <td><?= "Rp." . number_format($analisis['penyusutan1']) ?></td>
                          <td><?= "Rp." . number_format($analisis['penyusutan1']) ?></td>
                          <td><?= "Rp." . number_format($analisis['penyusutan1']) ?></td>
                          <td><?= "Rp." . number_format($analisis['penyusutan1']) ?></td>
                          <td><?= "Rp." . number_format($analisis['penyusutan1']) ?></td>
                        </tr>
                        <tr>
                          <td>7</td>
                          <td>Laba Sebelum Pajak</td>
                          <td>-</td>
                          <td><?= "Rp." . number_format($lsp1) ?></td>
                          <td><?= "Rp." . number_format($lsp2) ?></td>
                          <td><?= "Rp." . number_format($lsp3) ?></td>
                          <td><?= "Rp." . number_format($lsp3) ?></td>
                          <td><?= "Rp." . number_format($lsp3) ?></td>
                          <td><?= "Rp." . number_format($lsp3) ?></td>
                          <td><?= "Rp." . number_format($lsp3) ?></td>
                          <td><?= "Rp." . number_format($lsp3) ?></td>
                          <td><?= "Rp." . number_format($lsp3) ?></td>
                          <td><?= "Rp." . number_format($lsp3) ?></td>
                        </tr>
                        <tr>
                          <td>8</td>
                          <td>Pajak (<?= $configuration['percent_pajak'] . '%' ?>)</td>
                          <td>-</td>
                          <td><?= "Rp." . number_format($pajak1) ?></td>
                          <td><?= "Rp." . number_format($pajak2) ?></td>
                          <td><?= "Rp." . number_format($pajak3) ?></td>
                          <td><?= "Rp." . number_format($pajak3) ?></td>
                          <td><?= "Rp." . number_format($pajak3) ?></td>
                          <td><?= "Rp." . number_format($pajak3) ?></td>
                          <td><?= "Rp." . number_format($pajak3) ?></td>
                          <td><?= "Rp." . number_format($pajak3) ?></td>
                          <td><?= "Rp." . number_format($pajak3) ?></td>
                          <td><?= "Rp." . number_format($pajak3) ?></td>
                        </tr>
                        <tr>
                          <td>9</td>
                          <td>Laba Bersih</td>
                          <td>-</td>
                          <td><?= "Rp." . number_format($lb1) ?></td>
                          <td><?= "Rp." . number_format($lb2) ?></td>
                          <td><?= "Rp." . number_format($lb3) ?></td>
                          <td><?= "Rp." . number_format($lb3) ?></td>
                          <td><?= "Rp." . number_format($lb3) ?></td>
                          <td><?= "Rp." . number_format($lb3) ?></td>
                          <td><?= "Rp." . number_format($lb3) ?></td>
                          <td><?= "Rp." . number_format($lb3) ?></td>
                          <td><?= "Rp." . number_format($lb3) ?></td>
                          <td><?= "Rp." . number_format($lb3) ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<!-- /.container-fluid -->
</div>

<!-- modal delete -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Apa Anda Yakin?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">Data yang sudah dihapus tidak dapat dikembalikan!</div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
        <a id="btn-delete" class="btn btn-danger" href="#">Hapus</a>
      </div>
    </div>
  </div>
</div>