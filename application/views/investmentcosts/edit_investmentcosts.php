<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

    <div class="card col-lg-7 shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><a href="<?= base_url('InvestmentCosts') ?>"><i class="fas fa-arrow-left"></i> Kembali</a></h6>
        </div>
        <div class="card-body">
            <form action="" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?= $investmentcosts['id']; ?>" />
                <input type="hidden" name="created_at" value="<?= $investmentcosts['created_at']; ?>" />
                    <!-- edit title -->
                	<div class="form-group">
                		<label for="title">Nama Perhitungan Investasi</label>
                		<input class="form-control" type="text" name="title" placeholder="Perhitungan Investasi" value="<?= $investmentcosts['title'] ?>" />
                        <?= form_error('title', '<small class="text-danger pl-3">', '</small>'); ?>
                	</div>
                <!-- btn -->
                <input class="btn btn-success" type="submit" name="btn" value="Ubah" />
            </form>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->