<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

    <div class="card col-lg-7 shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><a href="<?= base_url('ReductionCosts') ?>"><i class="fas fa-arrow-left"></i> Kembali</a></h6>
        </div>
        <div class="card-body">
            <form action="" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?= $reductioncosts['id']; ?>" />
                <input type="hidden" name="created_at" value="<?= $reductioncosts['created_at']; ?>" />
                    <div class="form-group row">
                        <!-- category -->
                        <div class="col-lg-6">
                            <label for="category_id">Kategori</label>
                            <select name="category_id" id="category_id" class="form-control">
                                <option value="">Pilih Kategori</option>
                                <?php foreach ($category as $c) : ?>
                                    <option value="<?= $c['id']; ?>"><?= $c['title']; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <?= form_error('category_id', '<small class="text-danger pl-3">', '</small>'); ?>
                        </div>
                        <!-- ue th -->
                        <div class="col-lg-6">
                            <label for="ue_th">UE (th)</label>
                            <input class="form-control" type="text" name="ue_th" placeholder="UE (th)" value="<?= $reductioncosts['ue_th'] ?>" />
                            <?= form_error('ue_th', '<small class="text-danger pl-3">', '</small>'); ?>
                        </div>
                    </div>
                <!-- btn -->
                <input class="btn btn-success" type="submit" name="btn" value="Ubah" />
            </form>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->