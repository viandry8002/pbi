<script>
    function deleteConfirm(url){
        $('#btn-delete').attr('href', url);
        $('#deleteModal').modal();
    }
</script>

<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
    <!-- <a href="<?= base_url('LoanCosts/excel'); ?>" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-2"><i class="fas fa-download fa-sm text-white-50"></i> Export Data</a> -->
    <div class="col-lg-7">
        <?= form_error('title', '<div class="alert alert-danger" role="alert">', '</div>'); ?>
        <?= $this->session->flashdata('message'); ?>
    </div>
    <div class="card col-lg-12 shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><a href="" data-toggle="modal" data-target="#newLoanCostsModal"><i class="fas fa-plus"></i> Tambah Kewajiban Pinjaman</a></h6>
        </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="eksport" width="100%" cellspacing="0">
                        <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Tahun Pinjaman</th>
                                    <th>Total</th>
                                    <th>Angsuran</th>
                                    <th>Bungan <?= $configuration['interest_loan_costs'] ?>%</th>
                                    <th>Total Cicilan</th>
                                    <th>Aksi</th>
                                </tr>
                        </thead>
                    <tbody>
                        <?php $index = 1; ?>
                        <?php foreach($loancosts as $lc) : ?>
                        <tr>
                            <td><?= $index; ?></td>
                            <td><?= $lc['year_of_loan']; ?></td>
                            <td><?= "Rp." . number_format($lc['total']); ?></td>
                            <td><?= "Rp." . number_format($lc['instalment']); ?></td>
                            <td><?= "Rp." . number_format($lc['interest']); ?></td>
                            <td><?= "Rp." . number_format($lc['total_instalment']); ?></td>
                            <td>
                                <a class="badge badge-success" href="<?= site_url('LoanCosts/edit/'.$lc['id']); ?>">Ubah</a>
                                <a class="badge badge-danger" href="#!" onclick="deleteConfirm('<?= site_url('LoanCosts/delete/'.$lc['id']); ?>')">Hapus</a>
                            </td>
                        </tr>
                        <?php $index++; ?>
                        <?php endforeach; ?>
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<!-- /.container-fluid -->
</div>
<!-- Modal add new unit-->
<div class="modal fade" id="newLoanCostsModal" tabindex="-1" role="dialog" aria-labelledby="newLoanCostsModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="newLoanCostsModalLabel">Tambah Kewajiban Pinjaman</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <!-- form -->
      <form action="<?= site_url('LoanCosts/add'); ?>" method="post">
        <div class="modal-body">
          <div class="form-group row">
            <!-- input year -->
            <div class="col-lg-6">
              <label for="year_of_loan">Tahun</label>
              <select name="year_of_loan" id="year_of_loan" class="form-control">
                  <option value="">Pilih Tahun</option>
                  <option value="0">0</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>
                  <option value="10">10</option>
              </select>
            </div>
            <!-- input total -->
            <div class="col-lg-6">
              <label for="total">Total Pinjaman</label>
              <input value="0" type="text" class="form-control" id="total" name="total" placeholder="Total">
            </div>
          </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- modal delete -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Apa Anda Yakin?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">Data yang sudah dihapus tidak dapat dikembalikan!</div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
        <a id="btn-delete" class="btn btn-danger" href="#">Hapus</a>
      </div>
    </div>
  </div>
</div>