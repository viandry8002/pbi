<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

    <div class="card col-lg-7 shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><a href="<?= base_url('LoanCosts') ?>"><i class="fas fa-arrow-left"></i> Kembali</a></h6>
        </div>
        <div class="card-body">
            <form action="" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?= $loancosts['id']; ?>" />
                <input type="hidden" name="created_at" value="<?= $loancosts['created_at']; ?>" />
                    <!-- edit year_of_loan -->
                    <div class="form-group">
                        <label for="year_of_loan">Tahun</label>
                        <select name="year_of_loan" id="year_of_loan" class="form-control">
                            <option value="">Pilih Tahun</option>
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                        <?= form_error('year_of_loan', '<small class="text-danger pl-3">', '</small>'); ?>
                    </div>
                    <?php 
                    if ($loancosts['year_of_loan'] < 2) {
                    ?>    
                    <div class="form-group">
                        <label for="total">Total</label>
                        <input class="form-control" type="text" name="total" placeholder="Total" value="<?= $loancosts['total'] ?>" />
                    </div>
                    <?php
                    }
                    ?>
                <!-- btn -->
                <input class="btn btn-success" type="submit" name="btn" value="Ubah" />
            </form>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->