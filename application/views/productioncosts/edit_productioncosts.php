<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

    <div class="card col-lg-12 shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><a href="<?= base_url('ProductionCosts') ?>"><i class="fas fa-arrow-left"></i> Kembali</a></h6>
        </div>
        <div class="card-body">
            <form action="" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?= $productioncosts['id']; ?>" />
                <input type="hidden" name="created_at" value="<?= $productioncosts['created_at']; ?>" />
                    <!-- edit category_id -->
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label for="category_id">Kategori</label>
                            <select name="category_id" id="category_id" class="form-control">
                                <option value="">Pilih Kategori</option>
                                <?php foreach ($category as $ic) : ?>
                                <option value="<?= $ic['id']; ?>"><?= $ic['title']; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <?= form_error('category_id', '<small class="text-danger pl-3">', '</small>'); ?>
                        </div>
                        <div class="col-lg-6">
                            <label for="sub_category_id">Sub Kategori</label>
                            <select name="sub_category_id" id="sub_category_id" class="form-control">
                                <option value="">Pilih Sub Kategori</option>
                                <?php foreach ($subcategory as $scc) : ?>
                                <option value="<?= $scc['id']; ?>"><?= $scc['title']; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <?= form_error('sub_category_id', '<small class="text-danger pl-3">', '</small>'); ?>
                        </div>
                    </div>
                    <!-- edit year1 -->
                	<div class="form-group row">
                        <div class="col-lg-4">
                            <label for="year_1st">Tahun 1</label>
                            <input class="form-control" type="text" name="year_1st" placeholder="Tahun 1" value="<?= $productioncosts['year_1st'] ?>" />
                        </div>
                        <div class="col-lg-4">
                            <label for="year_2nd">Tahun 2</label>
                            <input class="form-control" type="text" name="year_2nd" placeholder="Tahun 2" value="<?= $productioncosts['year_2nd'] ?>" />
                        </div>
                        <div class="col-lg-4">
                            <label for="year_3rd">Tahun 3</label>
                            <input class="form-control" type="text" name="year_3rd" placeholder="Tahun 3" value="<?= $productioncosts['year_3rd'] ?>" />
                        </div>
                        <div class="col-lg-4">
                            <label for="year_4rd">Tahun 4</label>
                            <input class="form-control" type="text" name="year_4rd" placeholder="Tahun 4" value="<?= $productioncosts['year_4rd'] ?>" />
                        </div>
                        <div class="col-lg-4">
                            <label for="year_5rd">Tahun 5</label>
                            <input class="form-control" type="text" name="year_5rd" placeholder="Tahun 5" value="<?= $productioncosts['year_5rd'] ?>" />
                        </div>
                        <div class="col-lg-4">
                            <label for="year_6rd">Tahun 6</label>
                            <input class="form-control" type="text" name="year_6rd" placeholder="Tahun 6" value="<?= $productioncosts['year_6rd'] ?>" />
                        </div>
                        <div class="col-lg-4">
                            <label for="year_7rd">Tahun 7</label>
                            <input class="form-control" type="text" name="year_7rd" placeholder="Tahun 7" value="<?= $productioncosts['year_7rd'] ?>" />
                        </div>
                        <div class="col-lg-4">
                            <label for="year_8rd">Tahun 8</label>
                            <input class="form-control" type="text" name="year_8rd" placeholder="Tahun 8" value="<?= $productioncosts['year_8rd'] ?>" />
                        </div>
                        <div class="col-lg-4">
                            <label for="year_9rd">Tahun 9</label>
                            <input class="form-control" type="text" name="year_9rd" placeholder="Tahun 9" value="<?= $productioncosts['year_9rd'] ?>" />
                        </div>
                        <div class="col-lg-4">
                            <label for="year_10rd">Tahun 10</label>
                            <input class="form-control" type="text" name="year_10rd" placeholder="Tahun 10" value="<?= $productioncosts['year_10rd'] ?>" />
                        </div>
                	</div>
                <!-- btn -->
                <input class="btn btn-success" type="submit" name="btn" value="Ubah" />
            </form>
            <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
            <script>
            // when page is ready
            $(document).ready(function(){
                // when user selected
                $("#category_id").change(function(){
                $("#sub_category_id").hide(); // hide combo box
                // ajax
                $.ajax({
                    type: "POST", //post method
                    url: "<?php echo base_url("InvestmentCostsDetail/listSubCategory"); ?>", // url controller
                    data: {id_category : $("#category_id").val()}, // send data
                    dataType: "json",
                    beforeSend: function(e) {
                    if(e && e.overrideMimeType) {
                        e.overrideMimeType("application/json;charset=UTF-8");
                    }
                    },
                    success: function(response){ // process success
                    // set combo box
                    // show combo box
                    $("#sub_category_id").html(response.list_sub_category).show();
                    },
                    // handle error
                    error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // show alert
                    }
                });
                });
            });
            </script>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->