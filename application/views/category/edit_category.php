<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

    <div class="card col-lg-7 shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><a href="<?= base_url('Category') ?>"><i class="fas fa-arrow-left"></i> Kembali</a></h6>
        </div>
        <div class="card-body">
            <form action="" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?= $category['id']; ?>" />
                <input type="hidden" name="created_by" value="<?= $category['created_by']; ?>" />
                <input type="hidden" name="created_at" value="<?= $category['created_at']; ?>" />
                    <!-- edit type -->
                    <div class="form-group">
                        <label for="type">Tipe</label>
                        <select name="type" id="type" class="form-control">
                            <option value="">Pilih Tipe</option>
                            <option value="biaya_investasi">Biaya Investasi</option>
                            <option value="biaya_Lain">Biaya Lain</option>
                            <option value="biaya_tenaga_kerja">Biaya Tenaga Kerja</option>
                            <option value="biaya_produksi">Biaya Produksi</option>
                            <option value="analisa_rugi_laba">Analisa Rugi Laba</option>
                            <option value="arus_kas_pengeluaran_pemasukan">Arus Kas Pengeluaran Pemasukan</option>
                        </select>
                        <?= form_error('type', '<small class="text-danger pl-3">', '</small>'); ?>
                    </div>
                    <!-- edit title -->
                	<div class="form-group">
                		<label for="title">Nama Kategori</label>
                		<input class="form-control" type="text" name="title" placeholder="Kategori" value="<?= $category['title'] ?>" />
                        <?= form_error('title', '<small class="text-danger pl-3">', '</small>'); ?>
                	</div>
                <!-- btn -->
                <input class="btn btn-success" type="submit" name="btn" value="Ubah" />
            </form>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->