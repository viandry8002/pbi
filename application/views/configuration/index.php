<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

    <div class="col-lg-7">
        <?= form_error('title', '<div class="alert alert-danger" role="alert">', '</div>'); ?>
        <?= $this->session->flashdata('message'); ?>
    </div>

    <div class="col-md-8 mb-3">
        <div class="card shadow h-100 py-2">
            <div class="card-body">
                <form action="" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="<?= $configuration['id']; ?>" />
                    <input type="hidden" name="acceptance_value" value="<?= $configuration['acceptance_value']; ?>" />
                    <div class="form-group">
                        <label for="interest_loan_costs" class="col-form-label">Bunga Kewajiban Pinjaman</label>
                        <input type="text" class="form-control" id="interest_loan_costs" name="interest_loan_costs" value="<?= $configuration['interest_loan_costs'] ?>%">
                    </div>
                    <!-- input unit -->
                    <div class="form-group">
                        <label for="percent_pajak">Persen Pajak Analisis Rugi Laba</label>
                        <select name="percent_pajak" id="percent_pajak" class="form-control">
                            <option value="<?= $configuration['percent_pajak'] ?>"><?= $configuration['percent_pajak'] . '%' ?></option>
                            <option value="0">0</option>
                            <option value="10">10%</option>
                            <option value="20">20%</option>
                            <option value="30">30%</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="discount_rate" class="col-form-label">Discount Rate</label>
                        <input type="text" class="form-control" id="discount_rate" name="discount_rate" value="<?= $configuration['discount_rate'] ?>">
                    </div>
                    <div class="form-group justify-content-end">
                        <button type="submit" class="btn btn-primary">Ubah</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->