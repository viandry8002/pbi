<script>
    function deleteConfirm(url){
        $('#btn-delete').attr('href', url);
        $('#deleteModal').modal();
    }
</script>

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

    <div class="col-md-6">
        <?= $this->session->flashdata('message'); ?>
    </div>

    <div class="col-md-8 mb-3">
        <div class="card shadow h-100 py-2">
            <div class="card-body">
                <form class="user" method="post" action="<?= base_url('User/adduser'); ?>">
                    <div class="form-group">
                        <input type="text" class="form-control form-control-user" id="name" name="name" placeholder="Nama lengkap" 
                        value="<?= set_value('name'); ?>">
                        <?= form_error('name', '<small class="text-danger pl-3">', '</small>'); ?>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control form-control-user" id="email" name="email" placeholder="Alamat email"
                        value="<?= set_value('email'); ?>">
                        <?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <input type="password" class="form-control form-control-user" id="password1" name="password1" placeholder="Kata sandi">
                            <?= form_error('password1', '<small class="text-danger pl-3">', '</small>'); ?>
                        </div>
                        <div class="col-sm-6">
                            <input type="password" class="form-control form-control-user" id="password2" name="password2" placeholder="Ulangi kata sandi">
                        </div>
                    </div>
                    <!-- input role -->
                    <div class="col-lg-4">
                        <label for="role">Tipe Pengguna</label>
                        <select name="role" id="role" class="form-control">
                            <oproltion value="">Pilih Tipe</oproltion>
                            <?php foreach ($userrole as $ur) : ?>
                            <option value="<?= $ur['id']; ?>"><?= $ur['role']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-lg-4 mt-4">
                        <button type="submit" class="btn btn-primary btn-user btn-block">
                            Buat Akun
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- TABLE -->
    <div class="card col-lg-12 shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Daftar Pengguna</a></h6>
        </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Foto</th>
                                    <th>Tipe</th>
                                    <th>Bergabung Sejak</th>
                                    <th>Aksi</th>
                                </tr>
                        </thead>
                    <tbody>
                        <?php $index = 1; ?>
                        <?php foreach($alluser as $au) : ?>
                        <?php
                            if ($au['role_id'] != 1) {
                        ?>
                        <tr>
                            <td><?= $index; ?></td>
                            <td><?= $au['name'] ?></td>
                            <td><?= $au['email'] ?></td>
                            <td>
                                <img src="<?= base_url('assets/img/profile/') . $au['image']; ?>" width="64" class="img-thumbnail">
                            </td>
                            <td><?= $au['title_ur'] ?></td>
                            <td><?= date('d F Y' , $au['date_created']) ?></td>
                            <td>
                                <a class="badge badge-danger" href="#!" onclick="deleteConfirm('<?= site_url('User/deleteuser/'.$au['id']); ?>')">Hapus</a>
                            </td>
                        <?php
                            }
                        ?>
                        </tr>
                        <?php $index++; ?>
                        <?php endforeach; ?>
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<!-- modal delete -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Apa Anda Yakin?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">Akun yang sudah dihapus tidak dapat dikembalikan!</div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
        <a id="btn-delete" class="btn btn-danger" href="#">Hapus</a>
      </div>
    </div>
  </div>
</div>