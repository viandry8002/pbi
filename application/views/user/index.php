<script>
    function deleteConfirm(url){
        $('#btn-delete').attr('href', url);
        $('#deleteModal').modal();
    }
</script>

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

    <div class="col-md-6">
        <?= $this->session->flashdata('message'); ?>
    </div>

    <div class="col-md-8 mb-3">
        <div class="card shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="<?= base_url('assets/img/profile/') . $user['image']; ?>" class="card-img">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title"><?= $user['name']; ?></h5>
                            <p class="card-text"><?= $user['email']; ?></p>
                            <p class="card-text">
                            <small class="text-muted">Bergabung sejak <?= date('d F Y' , $user['date_created']); ?></small></p>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-lg-2">
                        <a class="badge badge-danger" href="#!" onclick="deleteConfirm('<?= site_url('user/delete/'.$user['id']); ?>')">Hapus Akun</a>
                    </div>
                    <div class="col-lg-2">
                        <a class="badge badge-primary" href="<?= base_url('User/edit'); ?>">Ubah Akun</a>
                    </div>
                    <div class="col-lg-2">
                        <a class="badge badge-success" href="<?= base_url('User/changepassword'); ?>">Ubah Kata Sandi</a>
                    </div>
                    <?php
                    if ($user['role_id'] == 1) {
                        ?>
                        <div class="col-lg-2">
                            <a class="badge badge-success" href="<?= base_url('User/adduser'); ?>">Tambah Pengguna</a>
                        </div>
                    <?php   
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->


<!-- modal delete -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Apa Anda Yakin?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">Akun yang sudah dihapus tidak dapat dikembalikan!</div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
        <a id="btn-delete" class="btn btn-danger" href="#">Hapus</a>
      </div>
    </div>
  </div>
</div>