<script>
    function deleteConfirm(url){
        $('#btn-delete').attr('href', url);
        $('#deleteModal').modal();
    }
</script>

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
    <!-- <a href="<?= base_url('EmployeeCosts/excel'); ?>" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm mb-2"><i class="fas fa-download fa-sm text-white-50"></i> Export Data</a> -->
    <div class="col-lg-7">
        <?= form_error('title', '<div class="alert alert-danger" role="alert">', '</div>'); ?>
        <?= $this->session->flashdata('message'); ?>
    </div>

    <!-- subtotal info -->
    <div class="row">
      <!-- sub total tk tidak lansung -->
      <div class="col-xl-4 col-md-6 mb-4">
          <div class="card border-left-secondary shadow h-100 py-2">
            <div class="card-body">
              <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                  <div class="text-xs font-weight-bold text-secondary text-uppercase mb-1">Tenaga Kerja Tidak Langsung</div>
                </div>
                <div class="col-auto">
                  <i class="fas fa-file-contract fa-2x text-gray-300"></i>
                </div>
              </div>
              <div class="row no-gutters align-items-center">
                  <div class="col-lg-4">
                    <div class="text-xs font-weight-bold text-secondary text-uppercase">Jumlah</div>
                  </div>
                  <div class="col-lg-6">
                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $jumlahtktidaklangsung ?></div>
                  </div>
                  <div class="col-lg-4">
                    <div class="text-xs font-weight-bold text-secondary text-uppercase">Biaya</div>
                  </div>
                  <div class="col-lg-6">
                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?= "Rp." . number_format($sttktidaklangsung) ?></div>
                  </div>
              </div>
            </div>
          </div>
      </div>
      <!-- sub total tk lansung -->
      <div class="col-xl-4 col-md-6 mb-4">
          <div class="card border-left-secondary shadow h-100 py-2">
            <div class="card-body">
              <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                  <div class="text-xs font-weight-bold text-secondary text-uppercase mb-1">Tenaga Kerja Langsung</div>
                </div>
                <div class="col-auto">
                  <i class="fas fa-file-contract fa-2x text-gray-300"></i>
                </div>
              </div>
              <div class="row no-gutters align-items-center">
                  <div class="col-lg-4">
                    <div class="text-xs font-weight-bold text-secondary text-uppercase">Jumlah</div>
                  </div>
                  <div class="col-lg-6">
                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $jumlahtklangsung ?></div>
                  </div>
                  <div class="col-lg-4">
                    <div class="text-xs font-weight-bold text-secondary text-uppercase">Biaya</div>
                  </div>
                  <div class="col-lg-6">
                    <div class="h5 mb-0 font-weight-bold text-gray-800"><?= "Rp." . number_format($sttklangsung) ?></div>
                  </div>
              </div>
            </div>
          </div>
      </div>
      <!-- sub total biaya tenaga kerja -->
      <div class="col-xl-4 col-md-6 mb-4">
        <div class="card border-left-info shadow h-100 py-2">
          <div class="card-body">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Total Biaya Tenaga Kerja</div>
                <div class="h5 mb-0 font-weight-bold text-gray-800"><?= "Rp." . number_format($sttk) ?></div>
              </div>
              <div class="col-auto">
                <i class="fas fa-file-contract fa-2x text-gray-300"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="card col-lg-12 shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><a href="" data-toggle="modal" data-target="#newEmployeeCostsModal"><i class="fas fa-plus"></i> Biaya Tenaga Kerja</a></h6>
        </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="eksport" width="100%" cellspacing="0">
                        <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Ketegori</th>
                                    <th>Sub Ketegori</th>
                                    <th>Jumlah</th>
                                    <th>Harga Satuan</th>
                                    <th>Biaya / Tahun</th>
                                    <th>Aksi</th>
                                </tr>
                        </thead>
                    <tbody>
                        <?php $index = 1; ?>
                        <?php foreach($employeecosts as $ec) : ?>
                        <tr>
                            <td><?= $index; ?></td>
                            <td><?= $ec['title_c'] ?></td>
                            <td><?= $ec['title_sc'] ?></td>
                            <td><?= $ec['total'] ?></td>
                            <td><?= "Rp." . number_format($ec['price_unit']) ?></td>
                            <td><?= "Rp." . number_format($ec['costs_year']) ?></td>
                            <td>
                                <a class="badge badge-success" href="<?= site_url('EmployeeCosts/edit/'.$ec['id']); ?>">Ubah</a>
                                <a class="badge badge-danger" href="#!" onclick="deleteConfirm('<?= site_url('EmployeeCosts/delete/'.$ec['id']); ?>')">Hapus</a>
                            </td>
                        </tr>
                        <?php $index++; ?>
                        <?php endforeach; ?>
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<!-- /.container-fluid -->
</div>
<!-- Modal add new employeecosts -->
<div class="modal fade" id="newEmployeeCostsModal" tabindex="-1" role="dialog" aria-labelledby="newEmployeeCostsModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="newEmployeeCostsModalLabel">Tambah Biaya Tenaga Kerja</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <!-- form -->
      <form action="<?= site_url('EmployeeCosts/add'); ?>" method="post">
        <div class="modal-body">
            <div class="form-group row">
                <div class="col-lg-6">
                  <label for="category_id">Kategori</label>
                  <select name="category_id" id="category_id" class="form-control">
                      <option value="">Pilih Kategori</option>
                      <?php foreach ($category as $c) : ?>
                          <option value="<?= $c['id']; ?>"><?= $c['title']; ?></option>
                      <?php endforeach; ?>
                  </select>
                </div>
                <div class="col-lg-6">
                  <label for="sub_category_id">Sub Kategori</label>
                    <select name="sub_category_id" id="sub_category_id" class="form-control">
                      <option value="">Pilih Sub Kategori</option>
                      <?php foreach ($subcategory as $sc) : ?>
                        <option value="<?= $sc['id']; ?>"><?= $sc['title']; ?></option>
                      <?php endforeach; ?>
                  </select>
                </div>
            </div>
            <div class="form-group">
                <label for="total">Jumlah</label>
                <input type="text" class="form-control" id="total" name="total" placeholder="Masukan Jumlah" value="0">
                <?= form_error('total', '<small class="text-danger pl-3">', '</small>'); ?>
            </div>
            <div class="form-group">
                <label for="price_unit">Harga Satuan</label>
                <input type="text" class="form-control" id="price_unit" name="price_unit" placeholder="Masukan Total" value='0'>
                <?= form_error('price_unit', '<small class="text-danger pl-3">', '</small>'); ?>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </div>
      </form>
      <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
      	<script>
          // when page is ready
          $(document).ready(function(){
            // when user selected
            $("#category_id").change(function(){
              $("#sub_category_id").hide(); // hide combo box
              // ajax
              $.ajax({
                type: "POST", //post method
                url: "<?php echo base_url("InvestmentCostsDetail/listSubCategory"); ?>", // url controller
                data: {id_category : $("#category_id").val()}, // send data
                dataType: "json",
                beforeSend: function(e) {
                  if(e && e.overrideMimeType) {
                    e.overrideMimeType("application/json;charset=UTF-8");
                  }
                },
                success: function(response){ // process success
                  // set combo box
                  // show combo box
                  $("#sub_category_id").html(response.list_sub_category).show();
                },
                // handle error
                error: function (xhr, ajaxOptions, thrownError) {
                  alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // show alert
                }
              });
            });
          });
        </script>
    </div>
  </div>
</div>

<!-- modal delete -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Apa Anda Yakin?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">Data yang sudah dihapus tidak dapat dikembalikan!</div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
        <a id="btn-delete" class="btn btn-danger" href="#">Hapus</a>
      </div>
    </div>
  </div>
</div>