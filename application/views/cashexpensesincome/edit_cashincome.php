<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

    <div class="card col-lg-12 shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><a href="<?= base_url('CashExpensesIncome') ?>"><i class="fas fa-arrow-left"></i> Kembali</a></h6>
        </div>
        <div class="card-body">
            <form action="" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?= $cashincome['id']; ?>" />
                <input type="hidden" name="category_id" value="<?= $cashincome['category_id']; ?>" />
                <input type="hidden" name="sub_category_id" value="<?= $cashincome['sub_category_id']; ?>" />
                <input type="hidden" name="created_at" value="<?= $cashincome['created_at']; ?>" />
                <div class="form-group row">
                    <!-- year 1 -->
                    <div class="col-lg-4">
                        <label for="zero_year">Tahun 0</label>
                        <input class="form-control" type="text" name="zero_year" value="<?= $cashincome['zero_year'] ?>" />
                    </div>
                    <!-- year 1 -->
                    <div class="col-lg-4">
                        <label for="1st_year">Tahun 1</label>
                        <input class="form-control" type="text" name="1st_year" value="<?= $cashincome['1st_year'] ?>" />
                    </div>
                    <!-- year 2 -->
                    <div class="col-lg-4">
                        <label for="2nd_year">Tahun 2</label>
                        <input class="form-control" type="text" name="2nd_year" value="<?= $cashincome['2nd_year'] ?>" />
                    </div>
                    <!-- year 2 -->
                    <div class="col-lg-4">
                        <label for="3rd_year">Tahun 3</label>
                        <input class="form-control" type="text" name="3rd_year" value="<?= $cashincome['3rd_year'] ?>" />
                    </div>
                    <!-- year 2 -->
                    <div class="col-lg-4">
                        <label for="4rd_year">Tahun 4</label>
                        <input class="form-control" type="text" name="4rd_year" value="<?= $cashincome['4rd_year'] ?>" />
                    </div>
                    <!-- year 2 -->
                    <div class="col-lg-4">
                        <label for="5rd_year">Tahun 5</label>
                        <input class="form-control" type="text" name="5rd_year" value="<?= $cashincome['5rd_year'] ?>" />
                    </div>
                    <!-- year 2 -->
                    <div class="col-lg-4">
                        <label for="6rd_year">Tahun 6</label>
                        <input class="form-control" type="text" name="6rd_year" value="<?= $cashincome['6rd_year'] ?>" />
                    </div>
                    <!-- year 2 -->
                    <div class="col-lg-4">
                        <label for="7rd_year">Tahun 7</label>
                        <input class="form-control" type="text" name="7rd_year" value="<?= $cashincome['7rd_year'] ?>" />
                    </div>
                    <!-- year 2 -->
                    <div class="col-lg-4">
                        <label for="8rd_year">Tahun 8</label>
                        <input class="form-control" type="text" name="8rd_year" value="<?= $cashincome['8rd_year'] ?>" />
                    </div>
                    <!-- year 2 -->
                    <div class="col-lg-4">
                        <label for="9rd_year">Tahun 9</label>
                        <input class="form-control" type="text" name="9rd_year" value="<?= $cashincome['9rd_year'] ?>" />
                    </div>
                    <!-- year 2 -->
                    <div class="col-lg-4">
                        <label for="10rd_year">Tahun 10</label>
                        <input class="form-control" type="text" name="10rd_year" value="<?= $cashincome['10rd_year'] ?>" />
                    </div>
                </div>
                <!-- btn -->
                <input class="btn btn-success" type="submit" name="btn" value="Ubah" />
            </form>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->