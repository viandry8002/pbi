<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

    <div class="card col-lg-6 shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><a href="<?= base_url('InvestmentCostsDetail') ?>"><i class="fas fa-arrow-left"></i> Kembali</a></h6>
        </div>
        <div class="card-body">
            <form action="" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?= $investmentcostsdetail['id']; ?>" />
                <input type="hidden" name="category_id" value="<?= $investmentcostsdetail['category_id']; ?>" />
                <input type="hidden" name="sub_category_id" value="<?= $investmentcostsdetail['sub_category_id']; ?>" />
                <input type="hidden" name="total" value="<?= $investmentcostsdetail['total']; ?>" />
                <label for="price_per_unit">Modal Kerja</label>
                <input class="form-control" type="text" name="price_per_unit" placeholder="Modal Kerja" value="<?= $investmentcostsdetail['price_per_unit'] ?>" />
                <?= form_error('price_per_unit', '<small class="text-danger pl-3">', '</small>'); ?>
                <!-- btn -->
                <input class="btn btn-success mt-4" type="submit" name="btn" value="Ubah" />
            </form>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->