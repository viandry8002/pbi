<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

    <div class="card col-lg-7 shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><a href="<?= base_url('AdditionalCosts') ?>"><i class="fas fa-arrow-left"></i> Kembali</a></h6>
        </div>
        <div class="card-body">
            <form action="" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?= $additionalcosts['id']; ?>" />
                <input type="hidden" name="created_at" value="<?= $additionalcosts['created_at']; ?>" />
                    <!-- edit category id -->
                    <div class="form-group">
                        <label for="category_id">Kategori</label>
                        <select name="category_id" id="category_id" class="form-control">
                            <option value="">Pilih Kategori</option>
                            <?php foreach ($category_biayalain as $cb) : ?>
                                <option value="<?= $cb['id']; ?>"><?= $cb['title']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <!-- edit sub category id -->
                    <div class="form-group">
                        <label for="sub_category_id">Sub Kategori</label>
                        <select name="sub_category_id" id="sub_category_id" class="form-control">
                            <option value="">Pilih Sub Kategori</option>
                            <?php foreach ($category_baiayainvest as $cbi) : ?>
                                <option value="<?= $cbi['id']; ?>"><?= $cbi['title']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                <!-- btn -->
                <input class="btn btn-success" type="submit" name="btn" value="Ubah" />
            </form>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->