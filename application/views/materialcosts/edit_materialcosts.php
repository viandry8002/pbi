<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

    <div class="card col-lg-7 shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><a href="<?= base_url('MaterialCosts') ?>"><i class="fas fa-arrow-left"></i> Kembali</a></h6>
        </div>
        <div class="card-body">
            <form action="" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?= $materialcosts['id']; ?>" />
                <input type="hidden" name="created_at" value="<?= $materialcosts['created_at']; ?>" />
                    <!-- edit title -->
                	<div class="form-group">
                		<label for="title">Deskripsi</label>
                		<input class="form-control" type="text" name="title" placeholder="Masukan Deskripsi" value="<?= $materialcosts['title'] ?>" />
                        <?= form_error('title', '<small class="text-danger pl-3">', '</small>'); ?>
                	</div>
                    <!-- edit total -->
                	<div class="form-group">
                		<label for="total">Jumlah</label>
                		<input class="form-control" type="text" name="total" placeholder="Masukan Jumlah" value="<?= $materialcosts['total'] ?>" />
                        <?= form_error('total', '<small class="text-danger pl-3">', '</small>'); ?>
                	</div>
                    <!-- edit price unit -->
                	<div class="form-group">
                		<label for="price_unit">Harga Satuan</label>
                		<input class="form-control" type="text" name="price_unit" placeholder="Masukan Hraga Satuan" value="<?= $materialcosts['price_unit'] ?>" />
                        <?= form_error('price_unit', '<small class="text-danger pl-3">', '</small>'); ?>
                	</div>
                <!-- btn -->
                <input class="btn btn-success" type="submit" name="btn" value="Ubah" />
            </form>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->